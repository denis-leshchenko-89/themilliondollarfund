<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('applications', function(Blueprint $table)
        {
            $table->dropForeign('applications_user_id_foreign');
            $table->dropColumn('user_id');
            $table->softDeletes()->after('updated_at');
        });

        Schema::create('user_application', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('application_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('admins', function(Blueprint $table)
        {
            $table->dropColumn('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_application');

        Schema::table('applications', function(Blueprint $table)
        {
            $table->bigInteger('user_id')->unsigned()->after('id');
            $table->dropColumn('deleted_at');
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('admins', function(Blueprint $table)
        {
            $table->softDeletes()->after('updated_at');
        });
    }
}
