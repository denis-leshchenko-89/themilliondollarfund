<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->string('company_name');
            $table->string('company_site');
            $table->string('company_location');
            $table->tinyInteger('company_employees');
            $table->string('company_markets');
            $table->text('company_concept');
            $table->text('company_product');
            $table->string('project_title');
            $table->integer('project_price');
            $table->string('project_subtitle')->nullable();
            $table->string('project_category');
            $table->string('project_subcategory')->nullable();
            $table->text('project_description');
            $table->string('project_video')->nullable();
            $table->json('data')->nullable();
            $table->enum('status', ['submitted', 'reviewed', 'approved', 'canceled'])->default('submitted')->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
