<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfileDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_data', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->string('name')->nullable();
            $table->string('linkedin')->nullable();
            $table->text('photo_url')->nullable();
            $table->text('worked_at')->nullable();
            $table->text('founded')->nullable();
            $table->longText('mini_resume')->nullable();
            $table->string('study_at')->nullable();
            $table->string('study_begin')->nullable();
            $table->string('study_end')->nullable();
            $table->string('study_degree')->nullable();
            $table->string('study_field')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_data');
    }
}
