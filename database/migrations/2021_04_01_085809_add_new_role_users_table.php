<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewRoleUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE applications MODIFY COLUMN status ENUM('created', 'submitted', 'reviewed', 'approved', 'canceled') NOT NULL DEFAULT 'created'");


        Schema::table('users', function (Blueprint $table) {
            $table->enum('role', ['investor', 'entrepreneur', 'agency'])->index()->after('id');
        });
        \Illuminate\Support\Facades\DB::statement("UPDATE users SET `role` = 'investor' WHERE `type` = '1'");
        \Illuminate\Support\Facades\DB::statement("UPDATE users SET `role` = 'entrepreneur' WHERE `type` = '2'");
        \Illuminate\Support\Facades\DB::statement("UPDATE users SET `role` = 'agency' WHERE `type` = '3'");

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('type');
        });

        Schema::table('profiles', function (Blueprint $table) {
            $table->boolean('is_credited')->unsigned()->default(0)->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedTinyInteger('type')->after('id');
        });

        \Illuminate\Support\Facades\DB::statement("UPDATE users SET `type` = '1' WHERE `role` = 'investor'");
        \Illuminate\Support\Facades\DB::statement("UPDATE users SET `type` = '2' WHERE `role` = 'entrepreneur'");
        \Illuminate\Support\Facades\DB::statement("UPDATE users SET `type` = '3' WHERE `role` = 'agency'");

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('role');
        });

        Schema::table('profiles', function (Blueprint $table) {
            $table->dropColumn('is_credited');
        });
    }
}
