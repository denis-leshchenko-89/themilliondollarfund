<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameProfileDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('profile_data', 'profiles');

//        Schema::table('users', function (Blueprint $table) {
//            $table->bigInteger('profile_id')->nullable()->unsigned()->after('id');
//            $table->foreign('profile_id')->references('id')->on('profiles');
//        });

//        \Illuminate\Support\Facades\DB::statement("UPDATE users SET profile_id = (SELECT id FROM profiles WHERE user_id = users.id)");

        Schema::table('profiles', function (Blueprint $table) {
            $table->bigInteger('user_id')->unsigned()->change();
            $table->dropColumn('name');
            $table->string('angel_group_name')->nullable()->after('user_id');
            $table->boolean('is_angel_group')->unsigned()->default(0)->after('user_id');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->string('name')->nullable()->after('user_id');
//            $table->string('user_id')->after('id');
            $table->dropColumn('angel_group_name');
            $table->dropColumn('is_angel_group');
            $table->dropForeign('profiles_user_id_foreign');
        });

//        \Illuminate\Support\Facades\DB::statement("UPDATE profiles SET user_id = (SELECT id FROM users WHERE profile_id = profiles.id)");

//        Schema::table('users', function (Blueprint $table) {
//            $table->dropForeign('users_profile_id_foreign');
//            $table->dropColumn('profile_id');
//        });

        Schema::rename('profiles', 'profile_data');
    }
}
