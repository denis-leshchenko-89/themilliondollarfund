<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeApplicationsTable extends Migration
{

    public function __construct()
    {
        #Unknown database type enum requested
//        Schema::registerCustomDoctrineType(Doctrine\DBAL\Types\StringType::class, 'enum', 'ENUM');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::table('applications', function(Blueprint $table)
//        {
//            $table->enum('status', ['created', 'submitted', 'reviewed', 'approved', 'canceled'])->default('created')->after('data')->change();
//        });
        DB::statement("ALTER TABLE applications MODIFY COLUMN status ENUM('created', 'submitted', 'reviewed', 'approved', 'canceled') NOT NULL DEFAULT 'created'");
        \Illuminate\Support\Facades\DB::statement("UPDATE applications SET status = 'created' WHERE status = 'submitted'");

        Schema::table('user_application', function (Blueprint $table) {
            $table->enum('role', ['member', 'admin', 'owner'])->default('member')->index()->after('application_id');
            $table->enum('type', ['founder', 'team'])->default('founder')->index()->after('application_id');

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('application_id')->references('id')->on('applications');
            $table->unique(['application_id', 'user_id']);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->boolean('is_invited')->default(0)->after('password');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('applications', function(Blueprint $table)
//        {
//            $table->enum('status', ['submitted', 'reviewed', 'approved', 'canceled'])->default('submitted')->after('data')->change();
//        });
        \Illuminate\Support\Facades\DB::statement("UPDATE applications SET status = 'submitted' WHERE status = 'created'");
        DB::statement("ALTER TABLE applications MODIFY COLUMN status ENUM('submitted', 'reviewed', 'approved', 'canceled') NOT NULL DEFAULT 'submitted'");

        Schema::table('user_application', function (Blueprint $table) {
            $table->dropColumn('role');
            $table->dropColumn('type');
            $table->dropForeign('user_application_application_id_foreign');
            $table->dropUnique('user_application_application_id_user_id_unique');
            $table->dropForeign('user_application_user_id_foreign');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('is_invited');
        });
    }
}
