<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlertProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->string('company_location')->nullable()->after('angel_group_name');
            $table->string('company_site')->nullable()->after('angel_group_name');
            $table->renameColumn('is_angel_group', 'is_company');
            $table->renameColumn('angel_group_name', 'company_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->renameColumn('is_company', 'is_angel_group');
            $table->renameColumn('company_name', 'angel_group_name');
            $table->dropColumn('company_location');
            $table->dropColumn('company_site');
        });
    }
}
