<?php


namespace Database\Seeders;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

/**
 * Class AdminSeeder
 * @package Database\Seeders
 */
class UserSeeder extends Seeder
{
    /**
     * @inheritdoc
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('profiles')->truncate();
        DB::table('users')->truncate();
        DB::table('users')->insert([
            'name' => 'admin',
            'role' => User::ENTREPRENEUR,
            'email' => 'admin@xicay.com',
            'password' => Hash::make('123456themilliondollarfund'),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}

