<?php

namespace Database\Seeders;

/**
 * Class Seeder
 * @package Database\Seeders
 */
abstract class Seeder extends \Illuminate\Database\Seeder
{
    /**
     * Seed database.
     */
    abstract public function run(): void;
}
