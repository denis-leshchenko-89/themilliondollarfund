<?php


namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

/**
 * Class AdminSeeder
 * @package Database\Seeders
 */
class AdminSeeder extends Seeder
{
    /**
     * @inheritdoc
     */
       public function run(): void
    {
        DB::table('admins')->truncate();
        DB::table('admins')->insert([
            'name' => 'admin',
            'email' => 'admin@xicay.com',
            'password' => Hash::make('123456themilliondollarfund'),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
