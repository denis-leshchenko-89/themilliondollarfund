# The Million Dollars Fund.

## Installation:

```
# Clone this repo to your local machine
git clone git@bitbucket.org:xicay_tech/themilliondollarfundlaravel.git
cd themilliondollarfundlaravel

# Install packages
composer install

# Create and configure .env file:
cp .env.example .env

# Run docker containers `docker-compose up`
./vendor/bin/sail up
# OR
# Run docker containers in the background
# `docker-compose up -d`
./vendor/bin/sail up -d

# Migrate database & seeders & storage link
./vendor/bin/sail artisan migrate:fresh --seed
./vendor/bin/sail artisan storage:link

# host
http://localhost:3000/
```
## Additional commands:
```
# Stop containers and remove containers, networks, etc.
./vendor/bin/sail down

# Run PHP CLI commands and return output
./vendor/bin/sail php --version

# Node and NPM
./vendor/bin/sail node --version
./vendor/bin/sail npm instal
```
