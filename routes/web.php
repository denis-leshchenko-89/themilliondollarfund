<?php

use Illuminate\Support\Facades\Route;

Route::get('/{any}', static function () {
    return view('index');
})->where('any', '.*');
