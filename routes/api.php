<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'MainController')->name('main');
Route::get('/categories', 'MainController@categories')->name('main.categories');
Route::match(['GET', 'POST'],'/applications', 'MainController@applications')->name('main.applications');

Route::group(['middleware' => 'guest'], static function () {
    Route::post('login', 'AuthController@login')->name('auth.login');
    Route::post('register', 'AuthController@register')->name('auth.register');
    Route::post('forgot-password', 'AuthController@forgotPassword')->name('password.email');
    Route::post('reset-password', 'AuthController@resetPassword')->name('password.reset');
});

Route::group(['middleware' => 'auth:sanctum'], static function () {
    Route::post('logout', 'AuthController@logout')->name('auth.logout');

    Route::group(['prefix' => 'profile'], function() {
        Route::get('/', 'UserController@getProfile')->name('profile.view');
        Route::post('/update', 'UserController@updateProfile')->name('profile.update');
        Route::post('/account/update', 'UserController@updateAccount')->name('profile.account.update');
        Route::post('/password/update', 'UserController@updatePassword')->name('profile.password.update');
        Route::post('/delete', 'UserController@destroy')->name('user.destroy');
    });

    Route::group(['prefix' => 'application'], function() {
        Route::get('/view/{application}', 'ApplicationController@show')->name('application.view')->middleware('can:view,application');
        Route::get('/lists', 'ApplicationController@index')->name('application.lists');
        Route::post('/create', 'ApplicationController@store')->name('application.create');
        Route::post('/update/{application}', 'ApplicationController@update')->name('application.update')->middleware('can:update,application');
        Route::post('/delete/{application}', 'ApplicationController@destroy')->name('application.destroy')->middleware('can:delete,application');
    });

    Route::group(['prefix' => 'messages'], function () {
        Route::get('/', 'MessagesController@index')->name('messages');
        Route::get('create', 'MessagesController@create')->name('messages.create');
        Route::post('/', 'MessagesController@store')->name('messages.store');
        Route::get('{id}', 'MessagesController@show')->name('messages.show');
        Route::put('{id}', 'MessagesController@update')->name('messages.update');
    });
});

Route::post('comingsoon','\App\Http\Controllers\FormsController@comingSoonRequest');
Route::post('contactus','\App\Http\Controllers\FormsController@contactUs');
