<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'guest:admin'], static function () {
    Route::get('/login', 'AuthController@login')->name('login');
    Route::post('/login', 'AuthController@authenticate')->name('authenticate');
});

Route::group(['middleware' => 'auth:admin'], static function () {
    Route::get('/', 'MainController')->name('home');
    Route::post('/logout', 'AuthController@logout')->name('logout');
    Route::get('/profile', function () { return redirect()->route('admin.admins.edit', ['admin' => Auth::user()->id]); })->name('profile');
    Route::resource('users', 'UserController');
    Route::resource('admins', 'AdminController');
    Route::resource('applications', 'ApplicationController');
    Route::resource('categories', 'CategoriesController');
});
