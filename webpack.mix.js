const mix = require('laravel-mix');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .options({
        extractVueStyles: true,
        globalVueStyles: 'resources/sass/_variables.scss',
        processCssUrls: false
    })
    .version();
//  mix.copy('resources/js/assets/images', 'public/images');
// mix.copy('resources/js/assets/fonts', 'public/fonts');
mix.browserSync({
    proxy: process.env.APP_URL,
    open: false
}

);
mix.autoload({
    jquery: ['$', 'window.jQuery']
});

