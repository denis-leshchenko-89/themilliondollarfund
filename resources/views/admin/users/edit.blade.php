@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12">
                <h1>{{ _('Edit') }}</h1>
            </div>
        </div>
    </div>
@stop

@section('content')
    @include('adminlte::common.errors')

    <div class="card">

        {!! Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'patch']) !!}

        <div class="card-body">
            <div class="row">
                @include('admin.users.fields')
            </div>
        </div>

        <div class="card-footer">
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            <a href="{{ route('admin.users.index') }}" class="btn btn-default">{{ _('Cancel') }}</a>
        </div>

       {!! Form::close() !!}

    </div>
@endsection
