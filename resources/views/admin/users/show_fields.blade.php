<!-- Type Field -->
<div class="col-sm-12">
    {!! Form::label('role', _('Role:')) !!}
    <p>{{ $user->roleName }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', _('Name:')) !!}
    <p>{{ $user->name }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('email', _('Email:')) !!}
    <p>{{ $user->email }}</p>
</div>

<!-- Created Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', _('Created:')) !!}
    <p>{{ $user->created_at }}</p>
</div>

<!-- Updated Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', _('Updated:')) !!}
    <p>{{ $user->updated_at }}</p>
</div>

@if ($user->profile)
    <div class="col-sm-12">
        <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">{{ _('Profile') }}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <div class="card-body">
                <div class="col-sm-12">
                    {!! Form::label('is_company', _('Angel group:')) !!}
                    <p>{{ $user->profile->is_company ? _('Yes') : _('No') }}</p>
                </div>
                <div class="col-sm-12">
                    {!! Form::label('company_name', _('Angel group name:')) !!}
                    <p>{{ $user->profile->company_name }}</p>
                </div>
                <div class="col-sm-12">
                    {!! Form::label('is_credited', _('Credited investor:')) !!}
                    <p>{{ $user->profile->is_credited ? _('Yes') : _('No') }}</p>
                </div>
                <div class="col-sm-12">
                    {!! Form::label('photo_url', _('Photo:')) !!}
                    <p>{{ $user->profile->photo_url }}</p>
                </div>
                <div class="col-sm-12">
                    {!! Form::label('study_at', _('Study at:')) !!}
                    <p>{{ $user->profile->study_at }}</p>
                </div>
                <div class="col-sm-12">
                    {!! Form::label('worked_at', _('Worked at:')) !!}
                    <p>{{ $user->profile->worked_at }}</p>
                </div>
                <div class="col-sm-12">
                    {!! Form::label('founded', _('Founded:')) !!}
                    <p>{{ $user->profile->founded }}</p>
                </div>
                <div class="col-sm-12">
                    {!! Form::label('mini_resume', _('Mini resume:')) !!}
                    <p>{{ $user->profile->mini_resume }}</p>
                </div>
                <div class="col-sm-12">
                    {!! Form::label('linkedin', _('Linkedin:')) !!}
                    <p>{{ $user->profile->linkedin }}</p>
                </div>
                <div class="col-sm-12">
                    {!! Form::label('study_begin', _('Study begin:')) !!}
                    <p>{{ $user->profile->study_begin }}</p>
                </div>
                <div class="col-sm-12">
                    {!! Form::label('study_end', _('Study end:')) !!}
                    <p>{{ $user->profile->study_end }}</p>
                </div>
                <div class="col-sm-12">
                    {!! Form::label('study_degree', _('Study degree:')) !!}
                    <p>{{ $user->profile->study_degree }}</p>
                </div>
                <div class="col-sm-12">
                    {!! Form::label('study_field', _('Study field:')) !!}
                    <p>{{ $user->profile->study_field }}</p>
                </div>
            </div>
                <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
@endif
