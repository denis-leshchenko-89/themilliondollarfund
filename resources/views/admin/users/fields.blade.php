<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('role', _('Role:')) !!}
    {!! Form::select('role', \App\Models\User::roles(), null, ['class' => 'form-control']) !!}
</div>


<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', _('Name:')) !!}
    {!! Form::text('name', null, ['class' => 'form-control','maxlength' => 191]) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', _('Email:')) !!}
    {!! Form::email('email', null, ['class' => 'form-control','maxlength' => 191]) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', _('Password:')) !!}
    {!! Form::password('password', ['class' => 'form-control','maxlength' => 191]) !!}
</div>

<div class="col-sm-12">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ _('Profile') }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="row">
                <div class="form-group col-sm-6 investor">
                    {!! Form::label('profile[is_company]', _('Angel group:')) !!}
                    {!! Form::select('profile[is_company]', \App\Models\Forms::boolean(), null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-6 investor">
                    {!! Form::label('profile[company_name]', _('Angel group name:')) !!}
                    {!! Form::text('profile[company_name]', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
                <div class="form-group col-sm-6 investor">
                    {!! Form::label('profile[is_credited]', _('Credited investor:')) !!}
                    {!! Form::select('profile[is_credited]', \App\Models\Forms::boolean(), null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('profile[photo_url]', _('Photo:')) !!}
                    {!! Form::text('profile[photo_url]', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('profile[study_at]', _('Study at:')) !!}
                    {!! Form::text('profile[study_at]', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('profile[worked_at]', _('Worked at:')) !!}
                    {!! Form::text('profile[worked_at]', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('profile[founded]', _('Founded:')) !!}
                    {!! Form::text('profile[founded]', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('profile[mini_resume]', _('Mini resume:')) !!}
                    {!! Form::text('profile[mini_resume]', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('profile[linkedin]', _('Linkedin:')) !!}
                    {!! Form::text('profile[linkedin]', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('profile[study_begin]', _('Study begin:')) !!}
                    {!! Form::text('profile[study_begin]', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('profile[study_end]', _('Study end:')) !!}
                    {!! Form::text('profile[study_end]', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('profile[study_degree]', _('Study degree:')) !!}
                    {!! Form::text('profile[study_degree]', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('profile[study_field]', _('Study field:')) !!}
                    {!! Form::text('profile[study_field]', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
