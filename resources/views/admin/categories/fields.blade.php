<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', _('Name:')) !!}
    {!! Form::text('name', null, ['class' => 'form-control','maxlength' => 191]) !!}
</div>

<!-- Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slug', _('Slug:')) !!}
    {!! Form::text('slug', null, ['class' => 'form-control','maxlength' => 191]) !!}
</div>

<!-- Sort Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sort', _('Sort:')) !!}
    {!! Form::number('sort', null, ['class' => 'form-control', 'step' => '1']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', _('Status:')) !!}
    {!! Form::select('status', \App\Models\Forms::boolean(), null, ['class' => 'form-control']) !!}
</div>
