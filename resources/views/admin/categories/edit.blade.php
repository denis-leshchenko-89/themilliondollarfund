@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12">
                <h1>{{ _('Edit') }}</h1>
            </div>
        </div>
    </div>
@stop

@section('content')
    @include('adminlte::common.errors')

    <div class="card">

        {!! Form::model($category, ['route' => ['admin.categories.update', $category->id], 'method' => 'patch']) !!}

        <div class="card-body">
            <div class="row">
                @include('admin.categories.fields')
            </div>
        </div>

        <div class="card-footer">
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
            <a href="{{ route('admin.categories.index') }}" class="btn btn-default">{{ _('Cancel') }}</a>
        </div>

       {!! Form::close() !!}

    </div>
@endsection
