<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', _('Name:')) !!}
    <p>{{ $category->name }}</p>
</div>

<!-- Slug Field -->
<div class="col-sm-12">
    {!! Form::label('slug', _('Slug:')) !!}
    <p>{{ $category->slug }}</p>
</div>

<!-- Sort Field -->
<div class="col-sm-12">
    {!! Form::label('sort', _('Sort:')) !!}
    <p>{{ $category->sort }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    {!! Form::label('status', _('Status:')) !!}
    <p>{{ $category->status ? _('Yes') : _('No') }}</p>
</div>
