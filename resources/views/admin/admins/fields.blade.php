<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', _('Name:')) !!}
    {!! Form::text('name', null, ['class' => 'form-control','maxlength' => 191]) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', _('Email:')) !!}
    {!! Form::email('email', null, ['class' => 'form-control','maxlength' => 191]) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', _('Password:')) !!}
    {!! Form::password('password', ['class' => 'form-control','maxlength' => 191]) !!}
</div>
