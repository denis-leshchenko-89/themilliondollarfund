<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', _('Name:')) !!}
    <p>{{ $admin->name }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('email', _('Email:')) !!}
    <p>{{ $admin->email }}</p>
</div>

<!-- Created Field -->
<div class="col-sm-12">
    {!! Form::label('created_at', _('Created:')) !!}
    <p>{{ $admin->created_at }}</p>
</div>

<!-- Updated Field -->
<div class="col-sm-12">
    {!! Form::label('updated_at', _('Updated:')) !!}
    <p>{{ $admin->updated_at }}</p>
</div>
