@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ _('Details') }}</h1>
            </div>
            <div class="col-sm-6">
                <a class="btn btn-default float-right"
                   href="{{ route('admin.admins.index') }}">
                    {{ _('Back') }}
                </a>
            </div>
        </div>
    </div>
@stop
@section('content')
    <div class="card">

        <div class="card-body">
            <div class="row">
                @include('admin.admins.show_fields')
            </div>
        </div>

    </div>
@endsection
