@section('css')
    @include('admin.layouts.datatables_css')
@endsection
<div class="table-responsive">
    {!! $dataTable->table(['width' => '100%', 'class' => 'table table-striped table-bordered']) !!}
</div>
@push('js')
    @include('admin.layouts.datatables_js')
    {!! $dataTable->scripts() !!}
@endpush
