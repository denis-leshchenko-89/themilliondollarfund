@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>{{ _('Applications') }}</h1>
            </div>
            <div class="col-sm-6">
                <a class="btn btn-primary float-right" href="{{ route('admin.applications.create') }}">
                    {{ _('Add New') }}
                </a>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="card">
        @include('flash::message')
        <div class="clearfix"></div>
        <!-- /.card-header -->
        <div class="card-body">
            @include('admin.applications.table')

            <div class="card-footer clearfix float-right">
                <div class="float-right">

                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
@endsection

