<div class="col-sm-6">
    {!! Form::label('status', _('Status:')) !!}
    <p>{{ $application->status }}</p>
</div>
<div class="col-sm-6">
    {!! Form::label('approved_price', _('Price approved:')) !!}
    <p>{{ $application->approved_price }}</p>
</div>
<div class="col-sm-6">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ _('Company') }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    {!! Form::label('company_name', _('Name:')) !!}
                    <p>{{ $application->company_name }}</p>
                </div>
                <div class="col-sm-6">
                    {!! Form::label('company_site', _('Site:')) !!}
                    <p>{{ $application->company_site }}</p>
                </div>
                <div class="col-sm-6">
                    {!! Form::label('company_location', _('Location:')) !!}
                    <p>{{ $application->company_location }}</p>
                </div>
                <div class="col-sm-6">
                    {!! Form::label('company_employees', _('Employees:')) !!}
                    <p>{{ $application->company_employees }}</p>
                </div>
                <div class="col-sm-6">
                    {!! Form::label('company_markets', _('Markets:')) !!}
                    <p>{{ $application->company_markets }}</p>
                </div>
                <div class="col-sm-6">
                    {!! Form::label('company_concept', _('Concept:')) !!}
                    <p>{{ $application->company_concept }}</p>
                </div>
                <div class="col-sm-6">
                    {!! Form::label('company_product', _('Product:')) !!}
                    <p>{{ $application->company_product }}</p>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<div class="col-sm-6">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ _('Project') }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    {!! Form::label('project_title', _('Name:')) !!}
                    <p>{{ $application->project_title }}</p>
                </div>
                <div class="col-sm-6">
                    {!! Form::label('project_price', _('Price:')) !!}
                    <p>{{ $application->project_price }}</p>
                </div>
                <div class="col-sm-6">
                    {!! Form::label('project_subtitle', _('Subtitle:')) !!}
                    <p>{{ $application->project_subtitle }}</p>
                </div>
                <div class="col-sm-6">
                    {!! Form::label('project_category', _('Category:')) !!}
                    <p>{{ $application->project_category }}</p>
                </div>
                <div class="col-sm-6">
                    {!! Form::label('project_subcategory', _('Subcategory:')) !!}
                    <p>{{ $application->project_subcategory }}</p>
                </div>
                <div class="col-sm-6">
                    {!! Form::label('project_description', _('Description:')) !!}
                    <p>{{ $application->project_description }}</p>
                </div>
                <div class="col-sm-6">
                    {!! Form::label('project_video', _('Video:')) !!}
                    <p>{{ $application->project_video }}</p>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<div class="col-sm-6">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ _('Founders') }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div data-role="dynamic-fields">
                        @foreach ($application->users()->wherePivot('type', \App\Models\Role::ROLE_FOUNDER)->get() as $user)
                            <div class="row">
                                <div class="col-sm-6">
                                    <p>{{ $user->email }}</p>
                                </div>
                                <div class="col-sm-6">
                                    <p>{{ $user->access->role }}</p>
                                </div>
                            </div>  <!-- /div.row -->
                        @endforeach
                    </div>  <!-- /div[data-role="dynamic-fields"] -->
                </div>  <!-- /div.col-md-12 -->
            </div>  <!-- /div.row -->
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<div class="col-sm-6">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ _('Teams') }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div data-role="dynamic-fields">
                        @foreach ($application->users()->wherePivot('type', \App\Models\Role::ROLE_TEAM)->get() as $user)
                            <div class="row">
                                <div class="col-sm-6">
                                    <p>{{ $user->email }}</p>
                                </div>
                                <div class="col-sm-6">
                                    <p>{{ $user->access->role }}</p>
                                </div>
                            </div>  <!-- /div.row -->
                        @endforeach
                    </div>  <!-- /div[data-role="dynamic-fields"] -->
                </div>  <!-- /div.col-md-12 -->
            </div>  <!-- /div.row -->
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<div class="col-sm-6">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ _('Investors') }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div data-role="dynamic-fields">
                        @if(isset($application->data['investors']))
                            <div class="row">
                                @foreach ($application->data['investors'] as $data)
                                    <div class="col-sm-6">
                                        <p>{{ $data['name'] }}</p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p>{{ $data['url'] }}</p>
                                    </div>
                                @endforeach
                            </div>  <!-- /div.row -->
                        @endif
                    </div>  <!-- /div[data-role="dynamic-fields"] -->
                </div>  <!-- /div.col-md-12 -->
            </div>  <!-- /div.row -->
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<div class="col-sm-6">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ _('Incubators') }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div data-role="dynamic-fields">
                        @if(isset($application->data['incubators']))
                            <div class="row">
                                @foreach ($application->data['incubators'] as $data)
                                    <div class="col-sm-6">
                                        <p>{{ $data['name'] }}</p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p>{{ $data['url'] }}</p>
                                    </div>
                                @endforeach
                            </div>  <!-- /div.row -->
                        @endif
                    </div>  <!-- /div[data-role="dynamic-fields"] -->
                </div>  <!-- /div.col-md-12 -->
            </div>  <!-- /div.row -->
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<div class="col-sm-6">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ _('Advisors') }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div data-role="dynamic-fields">
                        @if(isset($application->data['advisors']))
                            <div class="row">
                                @foreach ($application->data['advisors'] as $data)
                                    <div class="col-sm-6">
                                        <p>{{ $data['name'] }}</p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p>{{ $data['url'] }}</p>
                                    </div>
                                @endforeach
                            </div>  <!-- /div.row -->
                        @endif
                    </div>  <!-- /div[data-role="dynamic-fields"] -->
                </div>  <!-- /div.col-md-12 -->
            </div>  <!-- /div.row -->
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<div class="col-sm-6">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ _('Partners') }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div data-role="dynamic-fields">
                        @if(isset($application->data['partners']))
                            <div class="row">
                                @foreach ($application->data['partners'] as $data)
                                    <div class="col-sm-6">
                                        <p>{{ $data['name'] }}</p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p>{{ $data['url'] }}</p>
                                    </div>
                                @endforeach
                            </div>  <!-- /div.row -->
                        @endif
                    </div>  <!-- /div[data-role="dynamic-fields"] -->
                </div>  <!-- /div.col-md-12 -->
            </div>  <!-- /div.row -->
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<div class="col-sm-6">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ _('Documents') }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div data-role="dynamic-fields">
                        @foreach ($application->getMedia('document') as $document)
                            <div class="row">
                                <div class="col-sm-12">
                                    <a href="{{ $document->getUrl() }}">{{ $document->name }}</a>
                                </div>
                            </div>  <!-- /div.row -->
                        @endforeach
                    </div>  <!-- /div[data-role="dynamic-fields"] -->
                </div>  <!-- /div.col-md-12 -->
            </div>  <!-- /div.row -->
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
