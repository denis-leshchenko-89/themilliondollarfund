<div class="col-sm-6">
    <div class="form-group">
        {!! Form::label('status', _('Status:')) !!}
        {!! Form::select('status', \App\Models\Application::statuses(), null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group">
        {!! Form::label('approved_price', _('Price approved:')) !!}
        {!! Form::select('approved_price', \App\Models\Forms::boolean(), null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="col-sm-6">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ _('Company') }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="row">
                <div class="form-group col-sm-6">
                    {!! Form::label('company_name', _('Name:')) !!}
                    {!! Form::text('company_name', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('company_site', _('Site:')) !!}
                    {!! Form::text('company_site', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('company_location', _('Location:')) !!}
                    {!! Form::text('company_location', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('company_employees', _('Employees:')) !!}
                    {!! Form::select('company_employees', \App\Models\Application::employees(), null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('company_markets', _('Markets:')) !!}
                    {!! Form::text('company_markets', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('company_concept', _('Concept:')) !!}
                    {!! Form::text('company_concept', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('company_product', _('Product:')) !!}
                    {!! Form::text('company_product', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<div class="col-sm-6">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ _('Project') }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="row">
                <div class="form-group col-sm-6">
                    {!! Form::label('project_title', _('Name:')) !!}
                    {!! Form::text('project_title', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('project_price', _('Price:')) !!}
                    {!! Form::number('project_price', null, ['class' => 'form-control', 'step' => '0.1']) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('project_subtitle', _('Subtitle:')) !!}
                    {!! Form::text('project_subtitle', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('project_category', _('Category:')) !!}
                    {!! Form::text('project_category', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('project_subcategory', _('Subcategory:')) !!}
                    {!! Form::text('project_subcategory', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('project_description', _('Description:')) !!}
                    {!! Form::text('project_description', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('project_video', _('Video:')) !!}
                    {!! Form::text('project_video', null, ['class' => 'form-control','maxlength' => 191]) !!}
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<div class="col-sm-6">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ _('Founders') }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div data-role="dynamic-fields">
                        @php $i = 0; @endphp
                        @if(isset($application))
                            @foreach ($application->users()->wherePivot('type', \App\Models\Role::ROLE_FOUNDER)->get() as $user)
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        {!! Form::text('users['.\App\Models\Role::ROLE_FOUNDER.']['.$user->id.'][email]', $user->email, ['class' => 'form-control','maxlength' => 191, 'placeholder' => 'Email']) !!}
                                    </div>
                                    <div class="form-group col-sm-5">
                                        {!! Form::select('users['.\App\Models\Role::ROLE_FOUNDER.']['.$user->id.'][role]', \App\Models\Role::permissions(), $user->access->role, ['class' => 'form-control']) !!}
                                    </div>
                                    <div class="form-group col-sm-1">
                                        <button class="btn btn-danger" data-role="remove">
                                            <i class="fas fa-minus"></i>
                                        </button>
                                        <button class="btn btn-primary" data-role="add">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                                @php $i = ++$user->id; @endphp
                            @endforeach
                        @endif
                        <div class="row">
                            <div class="form-group col-sm-6">
                                {!! Form::text('users['.\App\Models\Role::ROLE_FOUNDER.']['.$i.'][email]', null, ['class' => 'form-control','maxlength' => 191, 'placeholder' => 'Email']) !!}
                            </div>
                            <div class="form-group col-sm-5">
                                {!! Form::select('users['.\App\Models\Role::ROLE_FOUNDER.']['.$i.'][role]', \App\Models\Role::permissions(), null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group col-sm-1">
                                <button class="btn btn-danger" data-role="remove">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button class="btn btn-primary" data-role="add">
                                    <i class="fas fa-plus"></i>
                                </button>
                            </div>
                        </div>
                          <!-- /div.row -->
                    </div>  <!-- /div[data-role="dynamic-fields"] -->
                </div>  <!-- /div.col-md-12 -->
            </div>  <!-- /div.row -->
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<div class="col-sm-6">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ _('Teams') }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div data-role="dynamic-fields">
                        @php $i = 0; @endphp
                        @if(isset($application))
                            @foreach ($application->users()->wherePivot('type', \App\Models\Role::ROLE_TEAM)->get() as $user)
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        {!! Form::text('users['.\App\Models\Role::ROLE_TEAM.']['.$user->id.'][email]', $user->email, ['class' => 'form-control','maxlength' => 191, 'placeholder' => 'Email']) !!}
                                    </div>
                                    <div class="form-group col-sm-5">
                                        {!! Form::select('users['.\App\Models\Role::ROLE_TEAM.']['.$user->id.'][role]', \App\Models\Role::permissions(), $user->access->role, ['class' => 'form-control']) !!}
                                    </div>
                                    <div class="form-group col-sm-1">
                                        <button class="btn btn-danger" data-role="remove">
                                            <i class="fas fa-minus"></i>
                                        </button>
                                        <button class="btn btn-primary" data-role="add">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </div>
                                </div>
                                @php $i = ++$user->id; @endphp
                            @endforeach
                        @endif
                        <div class="row">
                            <div class="form-group col-sm-6">
                                {!! Form::text('users['.\App\Models\Role::ROLE_TEAM.']['.$i.'][email]', null, ['class' => 'form-control','maxlength' => 191, 'placeholder' => 'Email']) !!}
                            </div>
                            <div class="form-group col-sm-5">
                                {!! Form::select('users['.\App\Models\Role::ROLE_TEAM.']['.$i.'][role]', \App\Models\Role::permissions(), null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group col-sm-1">
                                <button class="btn btn-danger" data-role="remove">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button class="btn btn-primary" data-role="add">
                                    <i class="fas fa-plus"></i>
                                </button>
                            </div>
                        </div>
                          <!-- /div.row -->
                    </div>  <!-- /div[data-role="dynamic-fields"] -->
                </div>  <!-- /div.col-md-12 -->
            </div>  <!-- /div.row -->
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<div class="col-sm-6">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ _('Investors') }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div data-role="dynamic-fields">
                        @php $dataKeys = range(0,0); @endphp
                        @if(isset($application) and !empty($application->data['investors']))
                            @php $dataKeys = range(0, count($application->data['investors']) - 1); @endphp
                        @endif
                        @foreach ($dataKeys as $key)
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    {!! Form::text('data[investors]['.$key.'][name]', null, ['class' => 'form-control','maxlength' => 191, 'placeholder' => 'Name']) !!}
                                </div>
                                <div class="form-group col-sm-5">
                                    {!! Form::text('data[investors]['.$key.'][url]', null, ['class' => 'form-control','maxlength' => 191, 'placeholder' => 'Linkedin']) !!}
                                </div>
                                <div class="form-group col-sm-1">
                                    <button class="btn btn-danger" data-role="remove">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                    <button class="btn btn-primary" data-role="add">
                                        <i class="fas fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        @endforeach
                        <!-- /div.row -->
                    </div>  <!-- /div[data-role="dynamic-fields"] -->
                </div>  <!-- /div.col-md-12 -->
            </div>  <!-- /div.row -->
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<div class="col-sm-6">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ _('Incubators') }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div data-role="dynamic-fields">
                        @php $dataKeys = range(0,0); @endphp
                        @if(isset($application) and !empty($application->data['incubators']))
                            @php $dataKeys = range(0, count($application->data['incubators']) - 1); @endphp
                        @endif
                        @foreach ($dataKeys as $key)
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    {!! Form::text('data[incubators]['.$key.'][name]', null, ['class' => 'form-control','maxlength' => 191, 'placeholder' => 'Name']) !!}
                                </div>
                                <div class="form-group col-sm-5">
                                    {!! Form::text('data[incubators]['.$key.'][url]', null, ['class' => 'form-control','maxlength' => 191, 'placeholder' => 'Linkedin']) !!}
                                </div>
                                <div class="form-group col-sm-1">
                                    <button class="btn btn-danger" data-role="remove">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                    <button class="btn btn-primary" data-role="add">
                                        <i class="fas fa-plus"></i>
                                    </button>
                                </div>
                            </div>  <!-- /div.row -->
                        @endforeach
                    </div>  <!-- /div[data-role="dynamic-fields"] -->
                </div>  <!-- /div.col-md-12 -->
            </div>  <!-- /div.row -->
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<div class="col-sm-6">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ _('Advisors') }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div data-role="dynamic-fields">
                        @php $dataKeys = range(0,0); @endphp
                        @if(isset($application) and !empty($application->data['advisors']))
                            @php $dataKeys = range(0, count($application->data['advisors']) - 1); @endphp
                        @endif
                        @foreach ($dataKeys as $key)
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    {!! Form::text('data[advisors]['.$key.'][name]', null, ['class' => 'form-control','maxlength' => 191, 'placeholder' => 'Name']) !!}
                                </div>
                                <div class="form-group col-sm-5">
                                    {!! Form::text('data[advisors]['.$key.'][url]', null, ['class' => 'form-control','maxlength' => 191, 'placeholder' => 'Linkedin']) !!}
                                </div>
                                <div class="form-group col-sm-1">
                                    <button class="btn btn-danger" data-role="remove">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                    <button class="btn btn-primary" data-role="add">
                                        <i class="fas fa-plus"></i>
                                    </button>
                                </div>
                            </div>  <!-- /div.row -->
                        @endforeach
                    </div>  <!-- /div[data-role="dynamic-fields"] -->
                </div>  <!-- /div.col-md-12 -->
            </div>  <!-- /div.row -->
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<div class="col-sm-6">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ _('Partners') }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div data-role="dynamic-fields">
                        @php $dataKeys = range(0,0); @endphp
                        @if(isset($application) and !empty($application->data['partners']))
                            @php $dataKeys = range(0, count($application->data['partners']) - 1); @endphp
                        @endif
                        @foreach ($dataKeys as $key)
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    {!! Form::text('data[partners]['.$key.'][name]', null, ['class' => 'form-control','maxlength' => 191, 'placeholder' => 'Name']) !!}
                                </div>
                                <div class="form-group col-sm-5">
                                    {!! Form::text('data[partners]['.$key.'][url]', null, ['class' => 'form-control','maxlength' => 191, 'placeholder' => 'Linkedin']) !!}
                                </div>
                                <div class="form-group col-sm-1">
                                    <button class="btn btn-danger" data-role="remove">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                    <button class="btn btn-primary" data-role="add">
                                        <i class="fas fa-plus"></i>
                                    </button>
                                </div>
                            </div>  <!-- /div.row -->
                        @endforeach
                    </div>  <!-- /div[data-role="dynamic-fields"] -->
                </div>  <!-- /div.col-md-12 -->
            </div>  <!-- /div.row -->
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<div class="col-sm-6">
    <!-- general form elements -->
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ _('Documents') }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div data-role="dynamic-fields">
                        @php $i = 0; @endphp
                        @if(isset($application))
                            @foreach ($application->getMedia('document') as $document)
                                <div class="row">
                                    <div class="form-group col-sm-11">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="file_{{$i}}" name="documents[{{$i}}][file]" value="{{ $document->id }}">
                                            <input type="hidden" value="{{$document->id}}" name="documents[{{$i}}][id]">
                                            <label class="custom-file-label" for="file_{{$i}}">{{ $document->name }}</label>
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-1">
                                        <button class="btn btn-danger" data-role="remove">
                                            <i class="fas fa-minus"></i>
                                        </button>
                                        <button class="btn btn-primary" data-role="add">
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </div>
                                </div>  <!-- /div.row -->
                                @php $i = ++$document->id; @endphp
                            @endforeach
                        @endif
                        <div class="row">
                            <div class="form-group col-sm-11">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="documents[{{$i}}][file]" name="documents[{{$i}}][file]">
                                    <label class="custom-file-label" for="documents[{{$i}}][file]">{{ _('Choose file') }}</label>
                                </div>
                            </div>

                            <div class="form-group col-sm-1">
                                <button class="btn btn-danger" data-role="remove">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button class="btn btn-primary" data-role="add">
                                    <i class="fas fa-plus"></i>
                                </button>
                            </div>
                        </div>  <!-- /div.row -->
                    </div>  <!-- /div[data-role="dynamic-fields"] -->
                </div>  <!-- /div.col-md-12 -->
            </div>  <!-- /div.row -->
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
@section('css')
    <style>
        [data-role="dynamic-fields"] > .row + .row {
            margin-top: 0.5em;
        }

        [data-role="dynamic-fields"] > .row [data-role="add"] {
            display: none;
        }

        [data-role="dynamic-fields"] > .row:last-child [data-role="add"] {
            display: inline-block;
        }

        [data-role="dynamic-fields"] > .row:last-child [data-role="remove"] {
            display: none;
        }
    </style>
@endsection

@push('js')
    <script>
        $(function() {
            // Remove button click
            $(document).on('click', '[data-role="dynamic-fields"] > .row [data-role="remove"]', function(e) {
                e.preventDefault();
                $(this).closest('.row').remove();
            });
            // Add button click
            $(document).on('click', '[data-role="dynamic-fields"] > .row [data-role="add"]', function(e) {
                e.preventDefault();
                let container = $(this).closest('[data-role="dynamic-fields"]');
                let new_field_group = container.children().filter('.row:last-child').clone();
                new_field_group.find('input').each(function(){
                    $(this).val('');
                });
                new_field_group.find('[name]').map((index, item) => {
                    item.name = item.name.replace(/\[(\d)\]/, `[${parseInt($(item).attr("name").match(/\[(\d)\]/)[1]) + 1}]`);
                    return item;
                });
                container.append(new_field_group);
            });
        });
    </script>
@endpush
