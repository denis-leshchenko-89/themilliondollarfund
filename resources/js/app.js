  
import 'es6-promise/auto';
import axios from 'axios';
import Vue from 'vue';
import _ from 'lodash';
import VueAxios from 'vue-axios';
import VueRouter from 'vue-router';
import Index from './Index';
import router from './router';
import store from "./store/index";
import Vuelidate from 'vuelidate'
import VueTextareaAutosize from 'vue-textarea-autosize'
import vuescroll from 'vuescroll';



Vue.use(vuescroll);

// Set Vue globally
window.Vue = Vue;
window.axios = axios;

// Load libraries
Vue.use(Vuelidate);
Vue.use(VueTextareaAutosize);

// Set Vue router
Vue.router = router;
Vue.use(VueRouter);

// Set Vue authentication
Vue.use(VueAxios, axios);

// Load Index
Vue.component('index', Index);


const app = new Vue({
  el: '#app',
  router,
  store
}).$mount('#app');