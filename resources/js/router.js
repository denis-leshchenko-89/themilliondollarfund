import VueRouter from 'vue-router'
import store from './store/index'
import Role from './role'


/* __________ IMPORT LANDING BEGIN __________*/
import HomePage from './pages/Landing/HomePage'
import AboutPage from './pages/Landing/AboutPage'
import HowItWorksForInvestorsPage from './pages/Landing/HowItWorksForInvestorsPage'
import HowItWorksForEntrepreneursPage from './pages/Landing/HowItWorksForEntrepreneursPage'
import ProjectsPage from './pages/Landing/ProjectsPage.vue'
import NotFound from './layout/NotFound'
/* __________ IMPORT LANDING END __________*/

/* __________ IMPORT AUTHORISATION BEGIN __________*/
import Registration from './pages/Auth/RegistrationPage'
import Login from './pages/Auth/LoginPage'
import ComingSoon from './pages/Auth/ComingSoonPage'

/* __________ IMPORT AUTHORISATION END __________*/

/* __________ IMPORT INVESTOR AND ENTREPRENEUR BEGIN __________*/

import ProfilePage from './pages/Entrepreneur/Profile/ProfilePage'
import ApplicationsPage from './pages/Entrepreneur/Applications/ApplicationsPage'
import CalculatorApplicationPage from './pages/Entrepreneur/Applications/CalculatorApplicationPage'
import CreateApplicationPage from './pages/Entrepreneur/Applications/CreateApplication/CreateApplicationPage'
import EditApplicationPage from './pages/Entrepreneur/Applications/EditApplication/EditApplicationPage'
import SettingsPage from './pages/Entrepreneur/Settings/SettingsPage'
import AccountInformationPage from './pages/Entrepreneur/Settings/AccountInformation/AccountInformationPage'
import ChangeYourPasswordPage from './pages/Entrepreneur/Settings/ChangeYourPassword/ChangeYourPasswordPage'
import DeactivateAccountPage from './pages/Entrepreneur/Settings/DeactivateAccount/DeactivateAccountPage'

/* __________ IMPORT INVESTOR AND ENTREPRENEUR END __________*/


/* __________ IMPORT MESSAGER BEGIN __________*/
import Messages from './pages/Messages/Messages'

/* __________ IMPORT MESSAGER END __________*/

const routes = [
    {
        path: '*',
        name: '404',
        component: NotFound
    },

    // {
    //     path: '/',
    //     name: 'coming-soon',
    //     component: ComingSoon
    // },

    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/registration',
        name: 'registration',
        component: Registration
    },


    /* __________ INVESTOR AND ENTREPRENEUR BEGIN __________*/
    {
        path: '/applications',
        name: 'applications',
        component: ApplicationsPage,
        meta: {
            requiresAuth: true,
            role: [Role.Entrepreneur]
        }
    },
    {
        path: '/application/calculator',
        name: 'calculator-application',
        component: CalculatorApplicationPage,
        meta: {
            requiresAuth: true,
            role: [Role.Entrepreneur]
        }
    },
    {
        path: '/application/create',
        name: 'create-application',
        component: CreateApplicationPage,
        meta: {
            requiresAuth: true,
            role: [Role.Entrepreneur]
        }
    },
    {
        path: '/application/edit/:id',
        name: 'edit-application',
        component: EditApplicationPage,
        meta: {
            requiresAuth: true,
            role: [Role.Entrepreneur]
        }
    },
    {
        path: '/profile',
        name: 'profile',
        component: ProfilePage,
        meta: {
            requiresAuth: true,
            role: [Role.Entrepreneur, Role.Investor]
        }
    },

    {
        path: '/settings',
        name: 'settings',
        component: SettingsPage,
        meta: {
            requiresAuth: true,
            role: [Role.Entrepreneur, Role.Investor]
        }
    },
    {
        path: '/settings/account-information',
        name: 'account-information',
        component: AccountInformationPage,
        meta: {
            requiresAuth: true,
            role: [Role.Entrepreneur, Role.Investor]
        }
    },

    {
        path: '/settings/change-your-password',
        name: 'change-your-password',
        component: ChangeYourPasswordPage,
        meta: {
            requiresAuth: true,
            role: [Role.Entrepreneur, Role.Investor]
        }
    },


    {
        path: '/settings/deactivate-account',
        name: 'deactivate-account',
        component: DeactivateAccountPage,
        meta: {
            requiresAuth: true,
            role: [Role.Entrepreneur, Role.Investor]
        }
    },




    /* __________  INVESTOR AND  ENTREPRENEUR END __________*/



    /* __________  MESSENGER BEGIN __________*/

    {
        path: '/messages',
        name: 'messages',
        component: Messages,
        meta: {
            requiresAuth: true,
            role: [Role.Entrepreneur, Role.Investor]
        }
    },

    /* __________  MESSENGER END __________*/

    /* __________  LANDING BEGIN __________*/
    {
        path: '/',
        name: 'home',
        component: HomePage,
        // meta: {
        //     requiresAuth: true,
        //     role: [Role.Entrepreneur, Role.Investor]
        // }
    },
    {
        path: '/about',
        name: 'about',
        component: AboutPage,
        // meta: {
        //     requiresAuth: true,
        //     role: [Role.Entrepreneur, Role.Investor]
        // }
    },
    {
        path: '/howitworksforinvestors',
        name: 'howitworksforinvestors',
        component: HowItWorksForInvestorsPage,
        // meta: {
        //     requiresAuth: true,
        //     role: [Role.Entrepreneur, Role.Investor]
        // }
    },
    {
        path: '/howitworksforentrepreneurs',
        name: 'howitworksforentrepreneurs',
        component: HowItWorksForEntrepreneursPage,
        // meta: {
        //     requiresAuth: true,
        //     role: [Role.Entrepreneur, Role.Investor]
        // }
    },
    {
        path: '/projects',
        name: 'projects',
        component: ProjectsPage,
        // meta: {
        //     requiresAuth: true,
        //     role: [Role.Entrepreneur, Role.Investor]
        // }
    }


    /* __________  LANDING END __________*/
]

const router = new VueRouter({
    history: true,
    mode: 'history',
    routes: routes
})

// router.beforeEach((to, from, next) => {
//     if (to.matched.some(record => record.meta.requiresAuth)) {
//         if (store.getters.isLoggedIn) {
//             next()
//         } else {
//             next('/')
//         }
//     } else {
//         next()
//     }
// })

// router.beforeEach((to, from, next) => {
//     if (to.matched.some((record) => record.meta.requiresAuth)) {
//         if (store.getters.isLoggedIn) {
//             if (to.matched.some((record) => record.meta.role)) {
//                 if (to.meta.role.some((role) => role == store.getters.getUserInfo.type )) {
//                     next()
//                 } else {
//                     next('/')
//                 }
//             } else {
//                 next('/')
//             }
//         } else {
//             next('/')
//         }
//     } else {
//         next()
//     }
// })





// router.beforeEach((to, from, next) => {
//     if (to.matched.some((record) => record.meta.requiresAuth)) {
//         if (store.getters.isLoggedIn) {
//             if (!store.getters.getUserInfo.role) {
//                 // console.log(store.getters.getUserInfo);
//                 store.dispatch('GETUSERINFO')
//                     .then((response) => {
//                         if (to.matched.some((record) => record.meta.role)) {
//                             if (to.meta.role.some((role) => role == store.getters.getUserInfo.role)) {
//                                 next()
//                             } else {
//                                 next('/')
//                             }
//                         }
//                         else {
//                             next('/')
//                         }

//                     })
//                     .catch((error) => {
//                         next('/')
//                     })
//             }
//             else {
//                 if (to.matched.some((record) => record.meta.role)) {
//                     if (to.meta.role.some((role) => role == store.getters.getUserInfo.role)) {
//                         next()
//                     } else {
//                         next('/')
//                     }
//                 }
//                 else {
//                     next('/')
//                 }
//             }
//         } else {
//             next('/')
//         }
//     } else {
//         next()
//     }
// })




router.beforeEach((to, from, next) => {
    if (to.matched.some((record) => record.meta.requiresAuth)) {
        if (store.getters.isLoggedIn) {
            if (!store.getters.getUserInfo.role) {
                store.dispatch('GETUSERINFO')
                    .then((response) => {
                        if (to.matched.some((record) => record.meta.role)) {
                            if (to.meta.role.some((role) => role == store.getters.getUserInfo.role)) {
                                next()
                            } else {
                                next('/')
                            }
                        }
                        else {
                            next('/')
                        }

                    })
                    .catch((error) => {
                        next('/')
                    })
            }
            else {
                if (to.matched.some((record) => record.meta.role)) {
                    if (to.meta.role.some((role) => role == store.getters.getUserInfo.role)) {
                        next()
                    } else {
                        next('/')
                    }
                }
                else {
                    next('/')
                }
            }
        } else {
            next('/')
        }
    } else {
        next()
    }
})

export default router
