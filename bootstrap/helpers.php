<?php

if (!function_exists('user_agent')) {
    /**
     * Returns user agent.
     *
     * @return string
     */
    function user_agent(): string
    {
        return request()->server('HTTP_USER_AGENT', 'Unknown');
    }
}
