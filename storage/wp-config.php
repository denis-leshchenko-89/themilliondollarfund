<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'applerestoration' );

/** MySQL database username */
define( 'DB_USER', 'dev' );

/** MySQL database password */
define( 'DB_PASSWORD', '2U&dm2NYC' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ')t8E(6w@BHaCt~-Tzgi9y&XVwIMGnyUKMhp:Wvr?LNe-Cp8Xtpzl8vXQ D}yJ?di' );
define( 'SECURE_AUTH_KEY',  'YE{1Z5]J*RV[8GRM]O%t e;*ub$pLf%c-P?%O|U?W-*g6~w$)o2&^hecM,XW79mQ' );
define( 'LOGGED_IN_KEY',    'hP0?Vs@O&|)7{L+Q~ZT4afR78_L3;A? ^;>?t*40aY]j&2-KR&t%$q))#`VAa15<' );
define( 'NONCE_KEY',        'jyuWX|@ILBRvaaKnX{nk:3#7dkI5v.y16zX!~fPVzl1sQe4):Au>}0pNFCs-:D.-' );
define( 'AUTH_SALT',        'Vs{n-gRb`XPqviYuEk1kX 2#aneF:,&1Wa83Z}`MuiX;+H_)B/eqUf7,sleZz[.V' );
define( 'SECURE_AUTH_SALT', '1eg>0)H=:;{_B}gb6UXFZ^J+iU6k,K;nL0lDX[iIy@$mDR>r.<3$DCLNPkGvKdcr' );
define( 'LOGGED_IN_SALT',   'ix+&bIYHre@bmGVh#DU=@G~[uwEg)|f!^>cT;GzR5V$S+ Yh|PT-3:S>*?/]D1:D' );
define( 'NONCE_SALT',       '/t{Y.mkF#X;<:JS%n{nP2_hZux]?afe;)hUs~@<<E^27SjgnM5~zSTo`m%ji^Hus' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = '0c5_';



define( 'AUTOSAVE_INTERVAL',    300  );
define( 'WP_POST_REVISIONS',    5    );
define( 'EMPTY_TRASH_DAYS',     7    );
define( 'WP_AUTO_UPDATE_CORE',  true );
define( 'WP_CRON_LOCK_TIMEOUT', 120  );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
