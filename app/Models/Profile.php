<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Profile
 *
 * @package App\Models
 */
class Profile extends Model
{
    use HasFactory;

    /**
     * @inheritdoc
     */
    protected $table = 'profiles';

    /**
     * @var string[]
     */
    protected $guarded = ['id'];

    /**
     * @var string[]
     */
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * @var string[]
     */
    public static $investorAttributes = [
            'name',
            'avatar',
            'company_name'
        ];

    /**
     * @var string[]
     */
    public static $entrepreneurAttributes = [
        'name',
        'linkedin',
        'avatar',
        'worked_at',
        'founded',
        'mini_resume',
        'study_at',
        'study_begin',
        'study_end',
        'study_degree',
        'study_field'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
