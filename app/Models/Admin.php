<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class Admin
 * @package App\Models\Admin
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 */
class Admin extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * @inheritdoc
     */
    protected $table = 'admins';

    /**
     * @inheritdoc
     */
    protected $guard = 'admin';

    /**
     * @inheritdoc
     */
    protected $fillable = ['name', 'email', 'password'];
}
