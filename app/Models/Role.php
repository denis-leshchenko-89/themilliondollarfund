<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Role extends Pivot
{
    public const PERMISSION_MEMBER = 'member';
    public const PERMISSION_ADMIN = 'admin';
    public const PERMISSION_OWNER = 'owner';

    public const ROLE_FOUNDER = 'founder';
    public const ROLE_TEAM = 'team';

    /**
     * @var string[]
     */
    protected $appends = [
        'label',
    ];

    /**
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Translation\Translator|string|null
     */
    public function getLabelAttribute()
    {
        return trans('roles'.$this->role);
    }

    /**
     * @param $roles
     *
     * @return bool
     */
    public function hasPermission($roles): bool
    {
        return in_array($this->role, (array) $roles);
    }

    /**
     * Available permissions.
     *
     * @return array
     */
    public static function permissions(): array
    {
        return [
            static::PERMISSION_ADMIN => _('Admin'),
            static::PERMISSION_MEMBER => _('Member'),
            static::PERMISSION_OWNER => _('Owner'),
        ];
    }
}
