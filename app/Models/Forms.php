<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Forms extends Model
{
    use HasFactory;

    /**
     * bool.
     *
     * @return array
     */
    public static function boolean(): array
    {
        return [
            0 => _('No'),
            1 => _('Yes'),
        ];
    }
}
