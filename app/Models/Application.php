<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Application extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia, SoftDeletes;

    public const STATUS_CREATED = 'created';
    public const STATUS_SUBMITTED = 'submitted';
    public const STATUS_REVIEWED = 'reviewed';
    public const STATUS_APPROVED = 'approved';
    public const STATUS_CANCELED = 'canceled';

    /**
     * @inheritdoc
     */
    protected $table = 'applications';

    protected $casts = [
        'data' => 'array'
    ];

    /**
     * @var string[]
     */
    protected $appends = ['documents', 'picture', 'roles'];

    /**
     * @var string[]
     */
    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'user_application')
            ->using(Role::class)
            ->as('access')
            ->withPivot(['role', 'type']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'category_application')->withPivot(['sort'])->orderBy('pivot_sort','desc');
    }

    /**
     * @return array
     */
    public function getRolesAttribute(): array
    {
        $roles = [];
        foreach ($this->users as $user) {
            $roles[$user->access->type][$user->id] = array_merge($user->access->toArray(), [
                'name'=>$user->name,
                'email'=>$user->email,
            ]);
        }

        return $roles;
    }

    /**
     * @param \Spatie\MediaLibrary\MediaCollections\Models\Media|null $media
     *
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(100)
            ->height(100)
            ->sharpen(10);

        $this->addMediaConversion('adaptive')
            ->greyscale()
            ->withResponsiveImages();
    }


    /**
     * @return mixed
     */
    public function getDocumentsAttribute()
    {
        $documents = [];
        if ($this->hasMedia('document')) {
            foreach ($this->getMedia('document') as $document) {
                $documents[] = [
                    'id' => $document->id,
                    'file_name' => $document->file_name,
                ];
            }
        }
        return $documents;
    }

    /**
     * @return mixed
     */
    public function getPictureAttribute()
    {
        if ($this->getFirstMedia('picture')) {
            return $this->getFirstMedia('picture')->getUrl();
        }

        return NULL;
    }

    /**
     * Available employees.
     *
     * @return array
     */
    public static function employees(): array
    {
        return [
            10 => '1-10',
            20 => '11-20',
            30 => '21-30',
            40 => '31-40',
            50 => '41-50',
            100 => '51-100',
        ];
    }

    /**
     * Available statuses.
     *
     * @return array
     */
    public static function statuses(): array
    {
        return [
            self::STATUS_CREATED => _('Created'),
            self::STATUS_SUBMITTED => _('Submitted'),
            self::STATUS_REVIEWED => _('Reviewed'),
            self::STATUS_CANCELED => _('Canceled'),
            self::STATUS_APPROVED => _('Approved'),
        ];
    }
}
