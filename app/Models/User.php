<?php

namespace App\Models;

use Cmgmyr\Messenger\Traits\Messagable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Sanctum\NewAccessToken;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * Class User
 * @package App\Models\User
 *
 * @property int $id
 * @property string $role
 * @property string $name
 * @property string $email
 */
class User extends Authenticatable implements HasMedia
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes, InteractsWithMedia, Messagable;

    public const INVESTOR = 'investor';

    public const ENTREPRENEUR = 'entrepreneur';

    public const AGENCY = 'agency';

    /**
     * @var string[]
     */
    protected $appends = ['roleName', 'completedOn', 'avatar'];

    /**
     * @inheritdoc
     */
    protected $fillable = ['name', 'email', 'password', 'role'];

    /**
     * @inheritdoc
     */
    protected $hidden = ['password', 'remember_token', 'email_verified_at', 'updated_at', 'deleted_at'];

    /**
     * @inheritdoc
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Available roles.
     *
     * @return array
     */
    public static function roles(): array
    {
        return [
            static::INVESTOR => _('Investor/Angel'),
            static::ENTREPRENEUR => _('Entrepreneur'),
            static::AGENCY => _('Agency'),
        ];
    }

    /**
     * @param \Spatie\MediaLibrary\MediaCollections\Models\Media|null $media
     *
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(100)
            ->height(100)
            ->sharpen(10);

        $this->addMediaConversion('adaptive')
            ->greyscale()
            ->withResponsiveImages();
    }

    /**
     * Find access token by name or create new one.
     *
     * @param string|null $user_agent
     *
     * @return NewAccessToken
     */
    public function findOrCreateToken(?string $user_agent = null): NewAccessToken
    {
        if (!$user_agent) {
            $user_agent = user_agent();
        }

        /** @noinspection MissedFieldInspection */
        $this->tokens()->where(['name' => $user_agent])->delete();

        return $this->createToken($user_agent);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile(): HasOne
    {
        return $this->hasOne(Profile::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function applications(): BelongsToMany
    {
        return $this->belongsToMany(Application::class, 'user_application')
            ->using(Role::class)
            ->as('access')
            ->withPivot(['role', 'type']);
    }

    /**
     * @return string
     */
    public function getRoleNameAttribute(): string {
        return self::roles()[$this->role];
    }

    /**
     * @return mixed
     */
    public function getAvatarAttribute()
    {
        if ($this->getFirstMedia('avatar')) {
            return $this->getFirstMedia('avatar')->getUrl('thumb');
        }

        return NULL;
    }

    /**
     * @return string
     */
    public function getCompletedOnAttribute(): string {
        $role = $this->role;
        if ($this->profile()->exists()) {
            if ($this->profile->is_company) {
                $role = User::INVESTOR;
            } else {
                $role = User::ENTREPRENEUR;
            }
            $data = collect($this->getAttribute('profile'))->merge($this->getAttributes())->merge(['avatar'=>$this->avatar]);
        }
        if ($role == User::INVESTOR) {
            $attributes = Profile::$investorAttributes;
        } else {
            $attributes = Profile::$entrepreneurAttributes;
        }

        $filled = 0;
        foreach ($attributes as $attribute) {
            if (!empty($data[$attribute])) {
                $filled++;
            }
        }

        return $filled / count($attributes) * 100;
    }
}
