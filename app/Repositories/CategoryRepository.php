<?php

namespace App\Repositories;

use App\Models\Category;
use App\Repositories\BaseRepository;

/**
 * Class CategoryRepository.
 */
class CategoryRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Category::class;
    }

    /**
     * @param \App\Models\Category $category
     * @param array            $attributes
     *
     * @return mixed
     */
    public function updateCategory(Category $category, array $attributes) {
        return $this->updateById($category->id, $attributes);
    }
}
