<?php

namespace App\Repositories;

use App\Models\Admin;
use App\Repositories\BaseRepository;

/**
 * Class UserRepository.
 */
class AdminRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Admin::class;
    }

    /**
     * @param \App\Models\Admin $admin
     * @param array            $attributes
     *
     * @return mixed
     */
    public function updateUser(Admin $admin, array $attributes) {
        return $this->updateById($admin->id, $attributes);
    }
}
