<?php

namespace App\Repositories;

use App\Models\Application;
use App\Models\Profile;
use App\Models\User;
use App\Repositories\BaseRepository;

/**
 * Class ApplicationRepository.
 */
class ApplicationRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Application::class;
    }
}
