<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\BaseRepository;

/**
 * Class UserRepository.
 */
class UserRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return User::class;
    }

    /**
     * @param \App\Models\User $user
     * @param array            $attributes
     *
     * @return mixed
     */
    public function updateUser(User $user, array $attributes) {
        return $this->updateById($user->id, $attributes);
    }

    /**
     * @param $item
     * @param $column
     *
     * @return mixed
     */
    public function getByColumnWithTrashed($item, $column) {
        return $this->model->withTrashed()->newQuery()->where($column, $item)->first();
    }
}
