<?php

namespace App\Repositories;

use App\Models\Profile;
use App\Models\User;
use App\Repositories\BaseRepository;

/**
 * Class ProfileRepository.
 */
class ProfileRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Profile::class;
    }

    /**
     * @param \App\Models\User $user
     * @param array            $attributes
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createByUser(User $user, array $attributes): \Illuminate\Database\Eloquent\Model {
        $this->fill($attributes);
        $this->model->user()->associate($user);
        $this->model->save();
        return $this->model;
    }
}
