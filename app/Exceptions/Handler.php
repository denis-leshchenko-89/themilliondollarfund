<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

/**
 * Class Handler
 * @package App\Exceptions
 */
class Handler extends ExceptionHandler
{
    /**
     * @inheritdoc
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register(): void
    {
        $this->reportable(static function (Throwable $e) {

        });
    }

    /**
     * {@inheritdoc}
     *
     * @return JsonResponse|mixed
     *
     * @throws Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($this->isApi($request)) {
            $code = $this->getCode($exception);
            $response = array_merge(['success' => false, 'code' => $code], $this->getMessage($exception));

            return new JsonResponse($response, $code);
        }

        return parent::render($request, $exception);
    }

    /**
     * Get code by exception.
     *
     * @param Throwable $exception
     *
     * @return int
     */
    protected function getCode(Throwable $exception): int
    {
        if (property_exists($exception, 'status')) {
            return $exception->status;
        }

        if ($exception instanceof AuthenticationException) {
            return 401;
        }

        if ($exception instanceof AuthorizationException) {
            return 403;
        }

        if ($exception instanceof ModelNotFoundException or $exception instanceof NotFoundHttpException) {
            return 404;
        }

        return $this->isHttpException($exception) && method_exists($exception, 'getStatusCode')
            ? $exception->getStatusCode() : 500;
    }

    /**
     * Get message by exception.
     *
     * @param Throwable $exception
     *
     * @return array
     */
    protected function getMessage(Throwable $exception): array
    {
        if ($exception instanceof ValidationException) {
            return ['message' => $exception->validator->getMessageBag()->toArray()];
        }
        if ($exception instanceof AuthenticationException) {
            return ['message' => 'Unauthenticated.'];
        }

        return $this->convertExceptionToArray($exception);
    }

    /**
     * Check if current request belongs to API.
     *
     * @param Request $request
     *
     * @return bool
     */
    private function isApi(Request $request): bool
    {
        return !empty($request->attributes->get('api', false));
    }

    /**
     * Convert an authentication exception into a response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return $request->expectsJson()
            ? response()->json(['message' => $exception->getMessage()], 401)
            : redirect()->guest($exception->redirectTo() ?? route('admin.login'));
    }
}
