<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

/**
 * Class RouteServiceProvider
 * @package App\Providers
 */
class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    protected string $baseNamespace = 'App\\Http\\Controllers';

    protected string $apiNamespace = 'App\\Http\\Controllers';

    protected string $backofficeNamespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->namespace($this->getApiNamespace())
                ->group(base_path('routes/api.php'));

            Route::prefix($this->getAdminPrefix())
                ->middleware('admin')
                ->namespace($this->getAdminNamespace())
                ->name('admin.')
                ->group(base_path('routes/admin.php'));

            Route::middleware('web')
                ->namespace($this->getNamespace())
                ->group(base_path('routes/web.php'));
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting(): void
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }

    /**
     * The controller namespace for the application.
     *
     * @return string
     */
    protected function getNamespace(): string
    {
        return 'App\Http\Controllers';
    }

    /**
     * The controller namespace for the API.
     *
     * @return string
     */
    protected function getApiNamespace(): string
    {
        return 'App\Http\Controllers\Api';
    }

    /**
     * The controller namespace for the BackOffice.
     *
     * @return string
     */
    protected function getAdminNamespace(): string
    {
        return 'App\Http\Controllers\Admin';
    }

    /**
     * The controller namespace for the BackOffice.
     *
     * @return string
     */
    protected function getAdminPrefix(): string
    {
        return config('app.admin_prefix');
    }
}
