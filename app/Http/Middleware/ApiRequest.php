<?php

namespace App\Http\Middleware;


use Closure;
use Illuminate\Http\Request;

/**
 * Class ApiRequest
 * @package App\Http\Middleware
 */
class ApiRequest
{
    /**
     * Handle request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $request->attributes->set('api', true);

        return $next($request);
    }
}
