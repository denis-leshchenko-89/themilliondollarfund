<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/**
 * Class Authenticate
 * @package App\Http\Middleware
 */
class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param Request $request
     * @return string|null
     */
    protected function redirectTo($request): ?string
    {
        if (!$request->expectsJson() && $request->is('admin')) {
            return route('admin.login');
        }

        if (!$request->expectsJson() && !$request->attributes->get('api', false)) {
            return Route::has('login') ? route('login') : '/';
        }

        return null;
    }
}
