<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Auth\ForgotPasswordRequest;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Requests\Api\Auth\RegisterRequest;
use App\Http\Requests\Api\Auth\ResetPasswordRequest;
use App\Jobs\User\CreateUserJob;
use App\Jobs\User\GetUserByEmailJob;
use App\Jobs\User\UpdateUserByIdJob;
use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

/**
 * Class AuthController
 * @package App\Http\Controllers\Api
 */
class AuthController extends Controller
{

    /**
     * @OA\Get(
     *    path="/me",
     *    tags={"Auth"},
     *    summary="Profile",
     *    description="Returns current authenticated user info.",
     *    @OA\Response(
     *        response=200,
     *        description="Current authenticated user info",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean", default="true"),
     *             @OA\Property(property="data", type="object",
     *               @OA\Property(property="id", type="int"),
     *               @OA\Property(property="type", type="int"),
     *               @OA\Property(property="name", type="string"),
     *               @OA\Property(property="email", type="string"),
     *               @OA\Property(property="created_at", type="string"),
     *             ),
     *             example={"id": 1, "type": 1, "name": "username", "email": "user@mail.com", "created_at": "2021-01-01T00:00:00.000000Z"}
     *         )
     *     ),
     *    @OA\Response(
     *        response=401,
     *        description="Unauthenticated error",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthenticated")
     *     ),
     * )
     */

    /**
     * User info.
     *
     * @return JsonResponse
     */
    public function me(): JsonResponse
    {
        return $this->sendResponse(Auth::user());
    }

    /**
     * @OA\Post(
     *    path="/register",
     *    tags={"Auth"},
     *    summary="Register",
     *    description="Create new user",
     *    @OA\RequestBody(@OA\JsonContent(
     *      required={"name", "email", "password", "type"},
     *      example={"name": "John Doe", "email": "user@mail.com", "password": "password", "type": 1},
     *      @OA\Property(property="name", type="string"),
     *      @OA\Property(property="email", type="string"),
     *      @OA\Property(property="password", type="string"),
     *      @OA\Property(property="type", type="int"),
     *    )),
     *
     *    @OA\Response(
     *        response=200,
     *        description="Info and API token for new created user",
     *        @OA\JsonContent(
     *           @OA\Property(property="success", type="boolean", default=true),
     *           @OA\Property(property="data", type="object",
     *             @OA\Property(ref="#/components/schemas/Authorized")
     *           ),
     *           example={"success": true, "data": {"name": "username", "email": "user@mail.com", "name": "username","email": "user@mail.com","token": {"access_token": "10|AaBbCcDdEe","token_type": "Bearer"}}}
     *        )
     *     ),
     *    @OA\Response(response=422, description="Error on data validation",
     *        @OA\JsonContent(
     *          example={"success": false, "code": 422, "message": {"password": "The password field is required."}}
     *       )
     *     ),
     * )
     */

    /**
     * Registration.
     *
     * @param RegisterRequest $request
     *
     * @return JsonResponse
     */
    public function register(RegisterRequest $request): JsonResponse
    {
        /** @var User $user */
        if ($user = $this->dispatchNow(new GetUserByEmailJob($request->get('email'), TRUE))) {
            $user->restore();
            $user = $this->dispatchNow(new UpdateUserByIdJob($user->id, $request->fields()));
        } elseif (!$user = $this->dispatchNow(new CreateUserJob($request->fields()))) {
            throw new ConflictHttpException('Failed to register new user');
        }

        Auth::login($user);
        return $this->ensureLogin($user);
    }

    /**
     * @OA\Post(
     *    path="/login",
     *    tags={"Auth"},
     *    summary="Login",
     *    description="Login user with email and password.",
     *    @OA\RequestBody(@OA\JsonContent(
     *      required={"email", "password"},
     *      example={"email": "user@mail.com", "password": "password"},
     *      @OA\Property(property="email", type="string"),
     *      @OA\Property(property="password", type="string"),
     *    )),
     *
     *    @OA\Response(
     *        response=200,
     *        description="User and API token info for authenticated user.",
     *        @OA\JsonContent(
     *           @OA\Property(property="success", type="boolean", default=true),
     *           @OA\Property(property="data", type="object",
     *             @OA\Property(ref="#/components/schemas/Authorized")
     *           ),
     *           example={"success": true, "data": {"name": "username", "email": "user@mail.com", "name": "username","email": "user@mail.com","token": {"access_token": "10|AaBbCcDdEe","token_type": "Bearer"}}}
     *        )
     *     ),
     *    @OA\Response(response=422, description="Error on data validation.",
     *        @OA\JsonContent(
     *          example={"success": false, "code": 422, "message": {"password": "The password field is required."}}
     *       )
     *     ),
     * )
     */

    /**
     * Authentication.
     *
     * @param LoginRequest $request
     *
     * @return JsonResponse
     *
     * @throws ValidationException
     */
    public function login(LoginRequest $request): JsonResponse
    {
        if (Auth::attempt($request->only('email', 'password'))) {
            return $this->ensureLogin(Auth::user());
        }

        throw ValidationException::withMessages([
            'email' => ['The provided credentials are incorrect.']
        ]);
    }

    /**
     * @OA\Post(
     *    path="/logout",
     *    tags={"Auth"},
     *    summary="Logout",
     *    description="Logout user",
     *
     *    @OA\Response(
     *        response=200,
     *        description="Successful",
     *        @OA\JsonContent(
     *           @OA\Property(property="success", type="boolean", default=true),
     *           @OA\Property(property="data", type="null"),
     *           example={"success": true, "data": null}
     *        )
     *     ),
     *    @OA\Response(
     *        response=401,
     *        description="Unauthenticated error",
     *         @OA\JsonContent(ref="#/components/schemas/Unauthenticated")
     *     ),
     * )
     */

    /**
     * Authentication.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        if ($user = Auth::user()) {
            $token = $user->currentAccessToken();
            /** @noinspection PhpUndefinedFieldInspection */
            if ($token && ($id = $token->id)) {
                /** @noinspection MissedFieldInspection */
                $user->tokens()->where(['id' => $id])->delete();
            }
        }
        return $this->sendResponse(null);
    }

    /**
     * resetPassword.
     *
     * @param ResetPasswordRequest $request
     *
     * @return JsonResponse
     */
    public function resetPassword(ResetPasswordRequest $request): JsonResponse
    {
        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) use ($request) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->tokens()->delete();

                $user->save();

                event(new PasswordReset($user));
            }
        );

        if ($status == Password::PASSWORD_RESET) {
            return $this->sendResponse(['status' => __($status)]);
        } else {
            return $this->sendError(['email' => [__($status)]]);
        }
    }


    /**
     * forgotPassword.
     *
     * @param ForgotPasswordRequest $request
     *
     * @return JsonResponse
     */
    public function forgotPassword(ForgotPasswordRequest $request): JsonResponse
    {
        $status = Password::sendResetLink(
            $request->only('email')
        );

        if ($status === Password::RESET_LINK_SENT) {
            return $this->sendResponse(['status' => __($status)]);
        } else {
            return $this->sendError(['email' => [__($status)]]);
        }
    }

    /**
     * Having authenticated user generate access token and send response.
     *
     * @param User $user
     *
     * @return JsonResponse
     */
    protected function ensureLogin(User $user): JsonResponse
    {
        if (!$token = $user->findOrCreateToken()) {
            throw new ConflictHttpException('Failed to authenticate user');
        }
        return $this->sendResponse([
            'userid' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'role' => $user->role,
            'token' => [
                'access_token' => $token->plainTextToken,
                'token_type' => 'Bearer',
            ],
        ]);
    }
}
