<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Applications\ApplicationRequest;
use App\Jobs\Application\CreateApplicationJob;
use App\Jobs\Application\DeleteApplicationByIdJob;
use App\Jobs\Application\UpdateApplicationByIdJob;
use App\Models\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

/**
 * Class ApplicationController
 *
 * @package App\Http\Controllers\Api
 */
class ApplicationController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        if (Auth::user()) {
            if (Auth::user()->applications) {
                return $this->sendResponse(collect(Auth::user()->applications)->except(['user', 'created_at', 'updated_at']));
            }
            return $this->sendResponse(NULL);
        } else {
            return $this->sendError(_('Not allowed'), 422);
        }
    }

    /**
     * @param \App\Models\Application $application
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Application $application): JsonResponse
    {
        try {
            return $this->sendResponse(collect($application)->except(['media']));
        } catch (\Exception $e) {
            return $this->sendError(_('Not found'), 422);
        }
    }

    /**
     * @param \App\Models\Application                                $application
     * @param \App\Http\Requests\Api\Applications\ApplicationRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Application $application, ApplicationRequest $request): JsonResponse
    {
        try {
            return $this->sendResponse($this->dispatchNow(new UpdateApplicationByIdJob($application->id, $request->fields(), Auth::user())));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $this->sendError(_('Can not update a entry'), 422);
    }

    /**
     * @param \App\Http\Requests\Api\Applications\ApplicationRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ApplicationRequest $request): JsonResponse
    {
        try {
            if (!$request->has('step') or $request->post('step') == 0) {
                return $this->sendResponse(NULL);
            }
            return $this->sendResponse($this->dispatchNow(new CreateApplicationJob($request->fields(), Auth::user())));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $this->sendError(_('Can not create a new entry'), 422);
    }

    /**
     * Remove the specified record from storage.
     *
     * @param  Application $application
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Application $application): JsonResponse
    {
        try {
            return $this->sendResponse($this->dispatchNow(new DeleteApplicationByIdJob($application->id)));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $this->sendError(_('Not allowed'), 422);
    }
}
