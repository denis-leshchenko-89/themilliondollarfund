<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\User\UpdateAccountRequest;
use App\Http\Requests\Api\User\UpdatePasswordRequest;
use App\Http\Requests\Api\User\UpdateProfileRequest;
use App\Jobs\Profile\CreateProfileByUserJob;
use App\Jobs\Profile\UpdateProfileByUserJob;
use App\Jobs\User\DeleteUserByIdJob;
use App\Jobs\User\UpdateUserByIdJob;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserController
 *
 * @package App\Http\Controllers\Api
 */
class UserController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProfile(): JsonResponse
    {
        if (Auth::user()) {
            return $this->sendResponse(collect(Auth::user())->except(['media']));
        } else {
            return $this->sendError(_('Not allowed'), 422);
        }
    }

    /**
     * @param \App\Http\Requests\Api\User\UpdateProfileRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProfile(UpdateProfileRequest $request): JsonResponse
    {
        try {
            return $this->sendResponse($this->dispatchNow(new UpdateUserByIdJob(Auth::id(), $request->validated())));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $this->sendError(_('Can not create or update a new entry'), 422);
    }

    /**
     * @param \App\Http\Requests\Api\User\UpdateAccountRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAccount(UpdateAccountRequest $request): JsonResponse
    {
        try {
            return $this->sendResponse($this->dispatchNow(new UpdateUserByIdJob(Auth::id(), $request->validated())));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $this->sendError(_('Can not create or update a new entry'), 422);
    }

    /**
     * @param \App\Http\Requests\Api\User\UpdatePasswordRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePassword(UpdatePasswordRequest $request): JsonResponse
    {
        try {
            return $this->sendResponse($this->dispatchNow(new UpdateUserByIdJob(Auth::id(), $request->fields())));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $this->sendError(_('Can not create or update a new entry'), 422);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy()
    {
        try {
            if ($is_removed = $this->dispatchNow(new DeleteUserByIdJob(Auth::id()))) {
                session()->flush();
            }

            return $this->sendResponse($is_removed);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return $this->sendError(_('Can not create or update a new entry'), 422);
    }
}
