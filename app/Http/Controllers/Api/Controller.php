<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\JsonResponse;


/**
 * Class Controller
 * @package App\Http\Controllers\Api
 */
abstract class Controller extends BaseController
{
    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="API Documentation",
     *      description="",
     * )
     *
     * @OA\Schema(
     *     schema="Authorized",
     *     description="Authorized user response (includes access token)",
     *     type="object",
     *     title="Authorized",
     *     type="object",
     *     @OA\Property(property="name", type="string"),
     *     @OA\Property(property="email", type="string"),
     *     @OA\Property(property="token", type="object",
     *       @OA\Property(property="access_token", type="string"),
     *       @OA\Property(property="token_type", type="string", default="Bearer"),
     *     ),
     *
     *     example={"name": "username","email": "user@mail.com","token": {"access_token": "10|AaBbCcDdEe","token_type": "Bearer"}}
     * )
     *
     * @OA\Schema(
     *     schema="Unauthenticated",
     *     description="Unauthorized error response",
     *     type="object",
     *     title="Unauthorized",
     *     type="object",
     *     @OA\Property(property="success", type="boolean", default=false),
     *     @OA\Property(property="code", type="int", default=401),
     *     @OA\Property(property="message", type="string", default="Unauthenticated."),
     *
     *     example={"success":false,"code":401,"message":"Unauthenticated."}
     * )
     */

    /**
     * Send successful response.
     *
     * @param mixed $data
     *
     * @return JsonResponse
     */
    protected function sendResponse($data): JsonResponse
    {
        return response()->json($data);
    }

    /**
     * Send error response.
     *
     * @param $data
     * @param $status
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendError($data, $status = 422): JsonResponse
    {
        return response()->json($data, $status);
    }
}
