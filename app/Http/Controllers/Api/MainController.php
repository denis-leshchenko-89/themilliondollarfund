<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Jobs\Application\GetApplicationsJob;
use App\Jobs\Category\GetCategoriesJob;
use Illuminate\Http\JsonResponse;
use Yajra\DataTables\DataTables;

/**
 * Class MainController
 *
 * @package App\Http\Controllers\Api
 */
class MainController extends Controller
{
    /**
     * @OA\Get(
     *    path="?",
     *    tags={"General"},
     *    summary="API",
     *    description="Returns API version",
     *    @OA\Response(
     *        response=200,
     *        description="Current API version",
     *         @OA\JsonContent(
     *             @OA\Property(property="success", type="boolean"),
     *             @OA\Property(property="data", type="object",
     *               @OA\Property(property="version", type="string"),
     *             ),
     *             example={"success": true, "data": {"version": "1.0.0"}}
     *         )
     *     ),
     * )
     */

    /**
     * API `homepage`.
     *
     * @return mixed
     */
    public function __invoke()
    {
        return $this->sendResponse(['version' => config('app.api_version')]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function categories(Request $request): JsonResponse
    {
        return $this->sendResponse($this->dispatchNow(new GetCategoriesJob(['status' => TRUE]))->sortBy('sort'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function applications(Request $request): JsonResponse
    {
        return $this->sendResponse(DataTables::of($this->dispatchNow(new GetApplicationsJob()))->make(true));
    }
}
