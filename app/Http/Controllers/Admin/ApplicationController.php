<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ApplicationDataTable;
use App\Http\Requests;
use App\Http\Requests\Admin\Application\CreateApplicationRequest;
use App\Http\Requests\Admin\Application\UpdateApplicationRequest;
use App\Jobs\Application\CreateApplicationJob;
use App\Jobs\Application\DeleteApplicationByIdJob;
use App\Jobs\Application\GetApplicationByIdJob;
use App\Jobs\Application\UpdateApplicationByIdJob;
use Flash;
use Response;

class ApplicationController extends Controller
{
    private $folderName = "applications";

    /**
     * Display a listing of the record.
     *
     * @param ApplicationDataTable $applicationDataTable
     * @return mixed
     */
    public function index(ApplicationDataTable $applicationDataTable)
    {
        return $applicationDataTable->render("admin.{$this->folderName}.index");
    }

    /**
     * Show the form for creating a new record.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view("admin.{$this->folderName}.create");
    }

    /**
     * Store a newly created record in storage.
     *
     * @param CreateApplicationRequest $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CreateApplicationRequest $request)
    {
        $this->dispatchNow(new CreateApplicationJob($request->fields()));

        Flash::success('Saved successfully.');

        return redirect(route("admin.{$this->folderName}.index"));
    }

    /**
     * Display the specified record.
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show($id)
    {
        $application = $this->dispatchNow(new GetApplicationByIdJob($id));

        if (empty($application)) {
            Flash::error('Not found');

            return redirect(route("admin.{$this->folderName}.index"));
        }

        return view("admin.{$this->folderName}.show")->with('application', $application);
    }

    /**
     * Show the form for editing the specified record.
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id)
    {
        $application = $this->dispatchNow(new GetApplicationByIdJob($id));

        if (empty($application)) {
            Flash::error('Not found');

            return redirect(route("admin.{$this->folderName}.index"));
        }

        return view("admin.{$this->folderName}.edit")->with('application', $application);
    }

    /**
     * Update the specified record in storage.
     *
     * @param  int              $id
     * @param UpdateApplicationRequest $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, UpdateApplicationRequest $request)
    {
        $application = $this->dispatchNow(new GetApplicationByIdJob($id));

        if (empty($application)) {
            Flash::error('Not found');

            return redirect(route("admin.{$this->folderName}.index"));
        }

        $this->dispatchNow(new UpdateApplicationByIdJob($application->id, $request->fields()));

        Flash::success('Updated successfully.');

        return redirect(route("admin.{$this->folderName}.index"));
    }

    /**
     * Remove the specified record from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $application = $this->dispatchNow(new GetApplicationByIdJob($id));

        if (empty($application)) {
            Flash::error('Not found');

            return redirect(route("admin.{$this->folderName}.index"));
        }

        $this->dispatchNow(new DeleteApplicationByIdJob($application->id));

        Flash::success('Deleted successfully.');

        return redirect(route("admin.{$this->folderName}.index"));
    }
}
