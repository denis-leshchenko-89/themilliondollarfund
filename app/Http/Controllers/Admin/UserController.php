<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\UserDataTable;
use App\Http\Requests;
use App\Http\Requests\Admin\User\CreateUserRequest;
use App\Http\Requests\Admin\User\UpdateUserRequest;
use App\Jobs\User\CreateUserJob;
use App\Jobs\User\DeleteUserByIdJob;
use App\Jobs\User\GetUserByIdJob;
use App\Jobs\User\UpdateUserByIdJob;
use Flash;
use Response;

class UserController extends Controller
{
    private $folderName = "users";
    /**
     * Display a listing of the record.
     *
     * @param UserDataTable $userDataTable
     * @return mixed
     */
    public function index(UserDataTable $userDataTable)
    {
        return $userDataTable->render("admin.{$this->folderName}.index");
    }

    /**
     * Show the form for creating a new record.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view("admin.{$this->folderName}.create");
    }

    /**
     * Store a newly created record in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CreateUserRequest $request)
    {
        $this->dispatchNow(new CreateUserJob($request->fields()));

        Flash::success('Saved successfully.');

        return redirect(route("admin.{$this->folderName}.index"));
    }

    /**
     * Display the specified record.
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show($id)
    {
        $user = $this->dispatchNow(new GetUserByIdJob($id));

        if (empty($user)) {
            Flash::error('Not found');

            return redirect(route("admin.{$this->folderName}.index"));
        }

        return view("admin.{$this->folderName}.show")->with('user', $user);
    }

    /**
     * Show the form for editing the specified record.
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id)
    {
        $user = $this->dispatchNow(new GetUserByIdJob($id));

        if (empty($user)) {
            Flash::error('Not found');

            return redirect(route("admin.{$this->folderName}.index"));
        }

        return view("admin.{$this->folderName}.edit")->with('user', $user);
    }

    /**
     * Update the specified record in storage.
     *
     * @param  int              $id
     * @param UpdateUserRequest $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, UpdateUserRequest $request)
    {
        $user = $this->dispatchNow(new GetUserByIdJob($id));

        if (empty($user)) {
            Flash::error('Not found');

            return redirect(route("admin.{$this->folderName}.index"));
        }

        $this->dispatchNow(new UpdateUserByIdJob($user->id, $request->fields()));

        Flash::success('Updated successfully.');

        return redirect(route("admin.{$this->folderName}.index"));
    }

    /**
     * Remove the specified record from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $user = $this->dispatchNow(new GetUserByIdJob($id));

        if (empty($user)) {
            Flash::error('Not found');

            return redirect(route("admin.{$this->folderName}.index"));
        }

        $this->dispatchNow(new DeleteUserByIdJob($user->id));

        Flash::success('Deleted successfully.');

        return redirect(route("admin.{$this->folderName}.index"));
    }
}
