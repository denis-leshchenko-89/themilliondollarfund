<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CategoryDataTable;
use App\Http\Requests;
use App\Http\Requests\Admin\Category\CreateCategoryRequest;
use App\Http\Requests\Admin\Category\UpdateCategoryRequest;
use App\Jobs\Category\CreateCategoryJob;
use App\Jobs\Category\DeleteCategoryByIdJob;
use App\Jobs\Category\GetCategoryByIdJob;
use App\Jobs\Category\UpdateCategoryByIdJob;
use Flash;
use Response;

class CategoriesController extends Controller
{
    private $folderName = "categories";
    /**
     * Display a listing of the record.
     *
     * @param CategoryDataTable $userDataTable
     * @return mixed
     */
    public function index(CategoryDataTable $userDataTable)
    {
        return $userDataTable->render("admin.{$this->folderName}.index");
    }

    /**
     * Show the form for creating a new record.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view("admin.{$this->folderName}.create");
    }

    /**
     * Store a newly created record in storage.
     *
     * @param CreateCategoryRequest $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CreateCategoryRequest $request)
    {
        $this->dispatchNow(new CreateCategoryJob($request->validated()));

        Flash::success('Saved successfully.');

        return redirect(route("admin.{$this->folderName}.index"));
    }

    /**
     * Display the specified record.
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show($id)
    {
        $category = $this->dispatchNow(new GetCategoryByIdJob($id));

        if (empty($category)) {
            Flash::error('Not found');

            return redirect(route("admin.{$this->folderName}.index"));
        }

        return view("admin.{$this->folderName}.show")->with('category', $category);
    }

    /**
     * Show the form for editing the specified record.
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id)
    {
        $category = $this->dispatchNow(new GetCategoryByIdJob($id));

        if (empty($category)) {
            Flash::error('Not found');

            return redirect(route("admin.{$this->folderName}.index"));
        }

        return view("admin.{$this->folderName}.edit")->with('category', $category);
    }

    /**
     * Update the specified record in storage.
     *
     * @param  int              $id
     * @param UpdateCategoryRequest $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, UpdateCategoryRequest $request)
    {
        $category = $this->dispatchNow(new GetCategoryByIdJob($id));

        if (empty($category)) {
            Flash::error('Not found');

            return redirect(route("admin.{$this->folderName}.index"));
        }

        $this->dispatchNow(new UpdateCategoryByIdJob($category->id, $request->fields()));

        Flash::success('Updated successfully.');

        return redirect(route("admin.{$this->folderName}.index"));
    }

    /**
     * Remove the specified record from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $category = $this->dispatchNow(new GetCategoryByIdJob($id));

        if (empty($category)) {
            Flash::error('Not found');

            return redirect(route("admin.{$this->folderName}.index"));
        }

        $this->dispatchNow(new DeleteCategoryByIdJob($category->id));

        Flash::success('Deleted successfully.');

        return redirect(route("admin.{$this->folderName}.index"));
    }
}
