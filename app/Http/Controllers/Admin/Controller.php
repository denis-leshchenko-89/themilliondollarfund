<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\JsonResponse;

/**
 * Class Controller
 * @package App\Http\Controllers\Admin
 */
abstract class Controller extends BaseController
{
    /**
     * Send successful response.
     *
     * @param mixed $data
     *
     * @return JsonResponse
     */
    protected function sendResponse($data): JsonResponse
    {
        return response()->json(['success' => true, 'data' => $data]);
    }

    /**
     * Send error response.
     *
     * @param $data
     * @param $status
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendError($data, $status): JsonResponse
    {
        return response()->json(['success' => false, 'data' => $data], $status);
    }
}
