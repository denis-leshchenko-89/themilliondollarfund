<?php

namespace App\Http\Controllers\Admin;

/**
 * Class MainController
 * @package App\Http\Controllers\Admin
 */
class MainController extends Controller
{
    /**
     * @return mixed
     */
    public function __invoke()
    {
        return view('admin.home');
    }

}
