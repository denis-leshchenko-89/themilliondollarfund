<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Auth\LoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

/**
 * Class AuthController
 * @package App\Http\Controllers\Admin
 */
class AuthController extends Controller
{

    /**
     * Display login page.
     *
     * @return mixed
     */
    public function login()
    {
        return view('adminlte::auth.login');
    }

    /**
     * Authenticate admin.
     *
     * @param LoginRequest $request
     *
     * @return mixed
     *
     * @throws ValidationException
     */
    public function authenticate(LoginRequest $request)
    {
        if (session()->has('password_hash_admin')) {
            session()->remove('password_hash_admin');
        }
        if (Auth::guard('admin')->attempt($request->only('email', 'password'))) {
            return redirect(route('admin.home'));
        }

        throw ValidationException::withMessages([
            'email' => ['The provided credentials are incorrect.']
        ]);
    }

    /**
     * Logout admin.
     *
     * @return mixed
     */
    public function logout()
    {
        Auth::guard('admin')->logout();

        return redirect(route('admin.login'));
    }
}
