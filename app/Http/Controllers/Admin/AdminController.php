<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\AdminDataTable;
use App\Http\Requests;
use App\Http\Requests\Admin\Admin\UpdateAdminRequest;
use App\Http\Requests\Admin\Admin\CreateAdminRequest;
use App\Jobs\Admin\CreateAdminJob;
use App\Jobs\Admin\DeleteAdminByIdJob;
use App\Jobs\Admin\GetAdminByIdJob;
use App\Jobs\Admin\UpdateAdminByIdJob;
use Flash;
use Response;

class AdminController extends Controller
{
    private $folderName = "admins";

    /**
     * Display a listing of the record.
     *
     * @param AdminDataTable $adminDataTable
     * @return mixed
     */
    public function index(AdminDataTable $adminDataTable)
    {
        return $adminDataTable->render("admin.{$this->folderName}.index");
    }

    /**
     * Show the form for creating a new record.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view("admin.{$this->folderName}.create");
    }

    /**
     * Store a newly created record in storage.
     *
     * @param CreateAdminRequest $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CreateAdminRequest $request)
    {
        $this->dispatchNow(new CreateAdminJob($request->fields()));

        Flash::success('Saved successfully.');

        return redirect(route("admin.{$this->folderName}.index"));
    }

    /**
     * Display the specified record.
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show($id)
    {
        $admin = $this->dispatchNow(new GetAdminByIdJob($id));

        if (empty($admin)) {
            Flash::error('Not found');

            return redirect(route("admin.{$this->folderName}.index"));
        }

        return view("admin.{$this->folderName}.show")->with('admin', $admin);
    }

    /**
     * Show the form for editing the specified record.
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function edit($id)
    {
        $admin = $this->dispatchNow(new GetAdminByIdJob($id));

        if (empty($admin)) {
            Flash::error('Not found');

            return redirect(route("admin.{$this->folderName}.index"));
        }

        return view("admin.{$this->folderName}.edit")->with('admin', $admin);
    }

    /**
     * Update the specified record in storage.
     *
     * @param  int              $id
     * @param UpdateAdminRequest $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, UpdateAdminRequest $request)
    {
        $admin = $this->dispatchNow(new GetAdminByIdJob($id));

        if (empty($admin)) {
            Flash::error('Not found');

            return redirect(route("admin.{$this->folderName}.index"));
        }

        $this->dispatchNow(new UpdateAdminByIdJob($admin->id, $request->fields()));

        Flash::success('Updated successfully.');

        return redirect(route("admin.{$this->folderName}.index"));
    }

    /**
     * Remove the specified record from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $admin = $this->dispatchNow(new GetAdminByIdJob($id));

        if (empty($admin)) {
            Flash::error('Not found');

            return redirect(route("admin.{$this->folderName}.index"));
        }

        $this->dispatchNow(new DeleteAdminByIdJob($admin->id));

        Flash::success('Deleted successfully.');

        return redirect(route("admin.{$this->folderName}.index"));
    }
}
