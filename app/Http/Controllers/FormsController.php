<?php

namespace App\Http\Controllers;

use App\Models\ComingSoon;
use Illuminate\Http\Request;
use App\Models\Forms;
use Illuminate\Support\Facades\Mail;

class FormsController extends Controller
{
    public function comingSoonRequest(request $request) {

        $data = json_encode($request->all());
        $forms = new Forms;
        $forms->formdata = $data;
        $forms->save();

        $data = array('name'=>$request->name, "email" => $request->email, "phone" => $request->phone, "role" => $request->role, "mess"=> $request->message);
        Mail::send('emails.ComingSoonRequest', $data, function($message) {
            $message->to(array(
                 'denis@xicay.com',
                 'bohdan@xicay.com',
                 'vitalii@xicay.com',
                 'xavier@xicay.com'
            ))
                ->subject('Coming Soon Request');
            $message->from(env('MAIL_FROM_ADDRESS'),'TMDF');
        });
        
    }
    
    public function contactUs(request $request) {
        $data = json_encode($request->all());
        $forms = new Forms;
        $forms->formdata = $data;
        $forms->save();

        $data = array('name'=>$request->name, "email" => $request->email, "company" => $request->company,"mess"=> $request->message);
        Mail::send('emails.ContactUs', $data, function($message) {
            $message->to(array(
                 'denis@xicay.com',
                 'bohdan@xicay.com',
                 'vitalii@xicay.com',
                 'xavier@xicay.com'
            ))
                ->subject('Contact Us');
            $message->from(env('MAIL_FROM_ADDRESS'),'TMDF');
        });
    }

}
