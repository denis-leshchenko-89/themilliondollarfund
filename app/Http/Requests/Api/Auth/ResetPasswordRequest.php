<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\Request;

/**
 * Class ResetPasswordRequest
 * @package App\Http\Requests\Api\Auth
 */
class ResetPasswordRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6|confirmed', //password_confirmation
        ];
    }
}
