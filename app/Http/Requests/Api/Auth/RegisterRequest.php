<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

/**
 * Class RegisterRequest
 * @package App\Http\Requests\Api\Auth
 */
class RegisterRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
            'password' => 'required|min:6',
            'role' => 'required|in:' . implode(',', array_keys(User::roles())),
            'profile.is_company' => 'nullable|boolean',
            'profile.is_credited' => 'nullable|boolean',
            'profile.company_name' => 'required_if:profile.is_company,1',
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields(): array
    {
        $result = parent::fields();
        $result['password'] = Hash::make($result['password']);

        return $result;
    }
}
