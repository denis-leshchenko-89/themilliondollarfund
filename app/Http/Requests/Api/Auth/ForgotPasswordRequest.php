<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\Request;

/**
 * Class ForgotPasswordRequest
 * @package App\Http\Requests\Api\Auth
 */
class ForgotPasswordRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email',
        ];
    }
}
