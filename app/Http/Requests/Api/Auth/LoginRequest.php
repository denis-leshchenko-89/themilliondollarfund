<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\Request;

/**
 * Class LoginRequest
 * @package App\Http\Requests\Api\Auth
 */
class LoginRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email',
            'password' => 'required'
        ];
    }
}
