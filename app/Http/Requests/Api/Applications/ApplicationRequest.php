<?php

namespace App\Http\Requests\Api\Applications;

use App\Http\Requests\Api\Request;
use App\Models\Application;
use App\Models\Role;

class ApplicationRequest extends Request
{

    /**
     * @return string[]
     */
    private function step1(): array {
        return ['project_price' => 'required|regex:/^\d{1,13}(\.\d{1,4})?$/'];
    }

    /**
     * @return string[]
     */
    private function step2(): array {
        return [
            'company_name'        => 'required|string|max:191',
            'company_site'        => 'required|url|max:191',
            'company_location'    => 'required|string',
            'company_employees'   => 'required|integer',
            'company_markets'     => 'required|string',
            'company_concept'     => 'required|string',
            'company_product'     => 'required|string',
            'project_title'       => 'required|string',
            'project_subtitle'    => 'nullable|string',
            'project_category'    => 'required|string',
            'project_subcategory' => 'nullable|string',
            'project_description' => 'required|string',
            'status'              => 'nullable|in:'.implode(',', [Application::STATUS_CREATED, Application::STATUS_SUBMITTED])
        ];
    }

    /**
     * @return string[]
     */
    private function step3(): array {
        return [
            'roles.founder.*.email'  => 'nullable|email',
            'roles.founder.*.role'   => 'nullable|in:'.implode(',', array_keys(Role::permissions())),
            'roles.team.*.email'     => 'nullable|email',
            'roles.team.*.role'      => 'nullable|in:'.implode(',', array_keys(Role::permissions())),
            'data'                   => 'nullable|array',
            'data.investors.*.name'  => 'nullable|string',
            'data.investors.*.url'   => 'nullable|url',
            'data.incubators.*.name' => 'nullable|string',
            'data.incubators.*.url'  => 'nullable|url',
            'data.advisors.*.name'   => 'nullable|string',
            'data.advisors.*.url'    => 'nullable|url',
            'data.partners.*.name'   => 'nullable|string',
            'data.partners.*.url'    => 'nullable|url',
            'status'                 => 'nullable|in:' . implode(',', [Application::STATUS_CREATED, Application::STATUS_SUBMITTED])
        ];
    }

    /**
     * @return string[]
     */
    private function step4(): array {
        return [
            'documents.*.file' => 'nullable|file',
            'documents.*.id'   => 'nullable|integer',
            'project_video'    => 'nullable|url',
            'project_media'    => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:22048',
            'status'           => 'required|in:'.implode(',',[Application::STATUS_CREATED, Application::STATUS_SUBMITTED])
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        switch ($this->step) {
            case 1: $rules = array_merge($this->step1(), $this->step2()); break;
            case 2: $rules = $this->step3(); break;
            case 3: $rules = array_merge($this->step1(), $this->step2(), $this->step3(), $this->step4()); break;
            default : $rules = $this->step1(); break;
        }

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function fields(): array
    {
        $result = parent::fields();
        if (isset($result['roles'])) {
            $result['users'] = $result['roles'];
        }

        return $result;
    }
}
