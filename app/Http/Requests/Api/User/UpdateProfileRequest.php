<?php

namespace App\Http\Requests\Api\User;

use App\Http\Requests\Api\Request;
use App\Models\User;

class UpdateProfileRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            'media' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:22048',
            'name' => 'required|string|max:191',
            'profile.is_company' => 'nullable|boolean',
            'profile.is_credited' => 'nullable|boolean',
            'profile.company_name' => 'required_if:is_company,1',
            'profile.study_at' => 'nullable|max:191',
            'profile.worked_at' => 'nullable|string',
            'profile.founded' => 'nullable|string',
            'profile.mini_resume' => 'nullable|string',
            'profile.linkedin' => 'nullable|url|max:191',
            'profile.study_begin' => 'nullable|max:191',
            'profile.study_end' => 'nullable|max:191',
            'profile.study_degree' => 'nullable|max:191',
            'profile.study_field' => 'nullable|max:191'
        ];
    }
}
