<?php

namespace App\Http\Requests\Api\User;

use App\Http\Requests\Api\Request;
use Illuminate\Support\Facades\Auth;

class UpdateAccountRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:191',
            'email' => 'required|email|unique:users,email,' . Auth::user()->id,
            'profile.company_name' => 'nullable|string',
            'profile.company_site' => 'nullable|url',
            'profile.company_location' => 'nullable|string',
        ];
    }
}
