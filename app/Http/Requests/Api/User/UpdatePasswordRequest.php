<?php

namespace App\Http\Requests\Api\User;

use App\Http\Requests\Api\Request;
use App\Rules\MatchOldPasswordValidation;
use Illuminate\Support\Facades\Hash;

/**
 * Class UpdatePasswordRequest
 * @package App\Http\Requests\Api\Auth
 */
class UpdatePasswordRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            'old_password' => ['required', new MatchOldPasswordValidation()],
            'password' => 'required|string|min:6|max:191|confirmed|different:old_password' //password_confirmation
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields(): array
    {
        $result = parent::fields();
        if (!empty($result['password'])) {
            $result['password'] = Hash::make($result['password']);
        }
        if (is_null($result['password'])) {
            unset($result['password']);
        }
        return $result;
    }
}
