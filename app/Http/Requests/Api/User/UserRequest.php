<?php

namespace App\Http\Requests\Api\User;

use App\Http\Requests\Api\Request;

/**
 * Class RegisterRequest
 * @package App\Http\Requests\Api\Auth
 */
class UserRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'email'],
            'password' => 'required'
        ];
    }
}
