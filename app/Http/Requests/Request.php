<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class Request
 * @package App\Http\Requests
 */
abstract class Request extends FormRequest
{
    /**
     * Available fields.
     *
     * @var array
     */
    protected array $fields = [];

    /**
     * Validation rules.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }

    /**
     * Validated fields.
     *
     * @return array
     */
    public function fields(): array
    {
        if (!$this->fields) {
            return $this->validated();
        }

        return array_filter($this->validated(), function (string $property) {
            return in_array($property, $this->fields, false);
        }, ARRAY_FILTER_USE_KEY);
    }

}
