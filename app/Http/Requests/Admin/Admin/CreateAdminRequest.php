<?php

namespace App\Http\Requests\Admin\Admin;

use App\Http\Requests\Admin\Request;
use Illuminate\Support\Facades\Hash;

class CreateAdminRequest extends Request
{

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:admins',
            'password' => 'required|min:6',
        ];

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function fields(): array
    {
        $result = parent::fields();
        $result['password'] = Hash::make($result['password']);

        return $result;
    }
}
