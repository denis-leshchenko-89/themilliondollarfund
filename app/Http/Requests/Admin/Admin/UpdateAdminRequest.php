<?php

namespace App\Http\Requests\Admin\Admin;

use App\Http\Requests\Admin\Request;
use Illuminate\Support\Facades\Hash;

class UpdateAdminRequest extends Request
{

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:admins,email,' . $this->route('admin'),
            'password' => 'nullable|min:6',
        ];

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function fields(): array
    {
        $result = parent::fields();
        if (!empty($result['password'])) {
            $result['password'] = Hash::make($result['password']);
        }
        if (is_null($result['password'])) {
            unset($result['password']);
        }
        return $result;
    }
}
