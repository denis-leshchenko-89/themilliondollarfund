<?php

namespace App\Http\Requests\Admin\Application;

use App\Http\Requests\Admin\Request;
use App\Models\Application;
use App\Models\Role;

class UpdateApplicationRequest extends Request
{

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        $rules = [
            'company_name' => 'nullable|string|max:191',
            'company_site' => 'nullable|url|max:191',
            'company_location' => 'nullable|string',
            'company_employees' => 'nullable|integer',
            'company_markets' => 'nullable|string',
            'company_concept' => 'nullable|string',
            'company_product' => 'nullable|string',
            'project_title' => 'nullable|string',
            'project_price' => 'required|regex:/^\d{1,13}(\.\d{1,4})?$/',
            'project_media' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:22048',
            'project_subtitle' => 'nullable|string',
            'project_category' => 'nullable|string',
            'project_subcategory' => 'nullable|string',
            'project_description' => 'nullable|string',
            'project_video' => 'url|nullable',
            'data' => 'nullable|array',
            'users.founder.*.email' => 'nullable|email',
            'users.founder.*.role' => 'nullable|in:' . implode(',', array_keys(Role::permissions())),
            'users.team.*.email' => 'nullable|email',
            'users.team.*.role' => 'nullable|in:' . implode(',', array_keys(Role::permissions())),
            'data.investors.*.name' => 'nullable|string',
            'data.investors.*.url' => 'nullable|url',
            'data.incubators.*.name' => 'nullable|string',
            'data.incubators.*.url' => 'nullable|url',
            'data.advisors.*.name' => 'nullable|string',
            'data.advisors.*.url' => 'nullable|url',
            'data.partners.*.name' => 'nullable|string',
            'data.partners.*.url' => 'nullable|url',
            'documents.*.file' => 'nullable|file',
            'documents.*.id' => 'nullable|integer',
            'status' => 'required|in:' . implode(',', array_keys(Application::statuses())),
            'approved_price' => 'nullable|boolean',
        ];

        return $rules;
    }
}
