<?php

namespace App\Http\Requests\Admin\User;

use App\Http\Requests\Admin\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UpdateUserRequest extends Request
{

    /**
     * @inheritdoc
     */
    public function rules(): array
    {

        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $this->route('user') .',id,deleted_at,NULL',
            'password' => 'nullable|min:6',
            'role' => 'required|in:' . implode(',', array_keys(User::roles())),
            'profile.is_company' => 'nullable|boolean',
            'profile.is_credited' => 'nullable|boolean',
            'profile.company_name' => 'required_if:profile.is_company,1',
            'profile.photo_url' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:22048',
            'profile.study_at' => 'nullable|max:191',
            'profile.worked_at' => 'nullable|string',
            'profile.founded' => 'nullable|string',
            'profile.mini_resume' => 'nullable|string',
            'profile.linkedin' => 'nullable|url|max:191',
            'profile.study_begin' => 'nullable|max:191',
            'profile.study_end' => 'nullable|max:191',
            'profile.study_degree' => 'nullable|max:191',
            'profile.study_field' => 'nullable|max:191'
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields(): array
    {
        $result = parent::fields();
        if (!empty($result['password'])) {
            $result['password'] = Hash::make($result['password']);
        }
        if (is_null($result['password'])) {
            unset($result['password']);
        }
        return $result;
    }
}
