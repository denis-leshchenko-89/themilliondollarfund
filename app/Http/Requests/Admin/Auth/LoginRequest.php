<?php

namespace App\Http\Requests\Admin\Auth;

use App\Http\Requests\Admin\Request;

/**
 * Class LoginRequest
 * @package App\Http\Requests\Admin\Auth
 *
 * @property string $email
 * @property string $password
 */
class LoginRequest extends Request
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'email'],
            'password' => 'required'
        ];
    }

}
