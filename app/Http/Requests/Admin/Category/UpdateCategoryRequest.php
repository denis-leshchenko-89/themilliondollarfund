<?php

namespace App\Http\Requests\Admin\Category;

use App\Http\Requests\Admin\Request;

class UpdateCategoryRequest extends Request
{

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'sort' => 'nullable|integer',
            'status' => 'required|boolean',
            'slug' => 'required|alpha_dash|unique:categories,slug,' . $this->route('category'),
        ];
    }
}
