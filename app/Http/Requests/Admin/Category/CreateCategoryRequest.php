<?php

namespace App\Http\Requests\Admin\Category;

use App\Http\Requests\Admin\Request;

class CreateCategoryRequest extends Request
{

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'sort' => 'nullable|integer',
            'status' => 'required|boolean',
            'slug' => 'nullable|alpha_dash|unique:categories',
        ];
    }
}
