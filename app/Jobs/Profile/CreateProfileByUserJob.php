<?php

namespace App\Jobs\Profile;

use App\Repositories\ProfileRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class CreateProfileByUserJob
 *
 * @package App\Jobs\Profile
 */
class CreateProfileByUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var \App\Models\User
     */
    protected \App\Models\User $user;

    /**
     * @var array
     */
    protected array $attributes;

    /**
     * CreateProfileJob constructor.
     *
     * @param \App\Models\User $user
     * @param array            $attributes
     */
    public function __construct(\App\Models\User $user, array $attributes)
    {
        $this->user = $user;
        $this->attributes = $attributes;
    }

    /**
     * @param \App\Repositories\ProfileRepository $profileRepository
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function handle(ProfileRepository $profileRepository): \Illuminate\Database\Eloquent\Model {
        return $profileRepository->createByUser($this->user, $this->attributes);
    }
}
