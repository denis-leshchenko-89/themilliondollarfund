<?php

namespace App\Jobs\Profile;

use App\Repositories\ProfileRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class UpdateProfileByUserJob
 *
 * @package App\Jobs\Profile
 */
class UpdateProfileByUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var \App\Models\User
     */
    protected \App\Models\User $user;

    /**
     * @var array
     */
    protected $attributes;

    /**
     * @var int
     */
    protected $id;

    /**
     * UpdateProfileByUserJob constructor.
     *
     * @param \App\Models\User $user
     * @param array            $attributes
     */
    public function __construct(\App\Models\User $user, array $attributes)
    {
        $this->user = $user;
        $this->attributes = $attributes;
    }

    /**
     * @param \App\Repositories\ProfileRepository $profileRepository
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function handle(ProfileRepository $profileRepository)
    {
        if ($this->user->profile()->exists()) {
            return $profileRepository->updateById($this->user->profile->id, $this->attributes);
        }

        return $profileRepository->createByUser($this->user, $this->attributes);
    }
}
