<?php

namespace App\Jobs\Admin;

use App\Repositories\AdminRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class CreateAdminJob
 *
 * @package App\Jobs\Frontend
 */
class CreateAdminJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    protected $attributes;

    /**
     * CreateAdminJob constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @param \App\Repositories\AdminRepository $adminRepository
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function handle(AdminRepository $adminRepository): \Illuminate\Database\Eloquent\Model {
        return $adminRepository->create($this->attributes);
    }
}
