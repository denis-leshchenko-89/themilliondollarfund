<?php

namespace App\Jobs\Admin;

use App\Repositories\AdminRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class UpdateAdminByIdJob
 *
 * @package App\Jobs\Frontend
 */
class UpdateAdminByIdJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    protected $attributes;

    /**
     * @var int
     */
    protected $id;

    /**
     * UpdateAdminByIdJob constructor.
     *
     * @param int   $id
     * @param array $attributes
     */
    public function __construct(int $id, array $attributes)
    {
        $this->id = $id;
        $this->attributes = $attributes;
    }

    /**
     * @param \App\Repositories\AdminRepository $adminRepository
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function handle(AdminRepository $adminRepository)
    {
        return $adminRepository->updateById($this->id, $this->attributes);
    }
}
