<?php

namespace App\Jobs\Admin;

use App\Repositories\AdminRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class DeleteAdminByIdJob
 *
 * @package App\Jobs\User
 */
class DeleteAdminByIdJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    protected $id;

    /**
     * DeleteAdminByIdJob constructor.
     *
     * @param $id
     */
    public function __construct($id) {
        $this->id = $id;
    }

    /**
     * @param \App\Repositories\AdminRepository $adminRepository
     *
     * @return bool|null
     * @throws \Exception
     */
    public function handle(AdminRepository $adminRepository): ?bool {
        return $adminRepository->deleteById($this->id);
    }
}
