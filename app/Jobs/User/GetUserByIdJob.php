<?php

namespace App\Jobs\User;

use App\Repositories\UserRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class GetUserByIdJob
 *
 * @package App\Jobs\Frontend
 */
class GetUserByIdJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    protected $id;

    /**
     * GetUserByIdJob constructor.
     *
     * @param $id
     */
    public function __construct($id) {
        $this->id = $id;
    }

    /**
     * @param \App\Repositories\UserRepository $userRepository
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function handle(UserRepository $userRepository)
    {
        return $userRepository->getById($this->id);
    }
}
