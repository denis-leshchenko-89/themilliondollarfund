<?php

namespace App\Jobs\User;

use App\Repositories\UserRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class GetUserByEmailJob
 *
 * @package App\Jobs\Frontend
 */
class GetUserByEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var bool
     */
    protected $trashed;

    /**
     * GetUserByEmailJob constructor.
     *
     * @param $email
     */
    public function __construct(string $email, bool $trashed = FALSE) {
        $this->email = $email;
        $this->trashed = $trashed;
    }

    /**
     * @param \App\Repositories\UserRepository $userRepository
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function handle(UserRepository $userRepository)
    {
        if ($this->trashed) {
            return $userRepository->getByColumnWithTrashed($this->email, 'email');
        } else {
            return $userRepository->getByColumn($this->email, 'email');
        }
    }
}
