<?php

namespace App\Jobs\User;

use App\Repositories\ProfileRepository;
use App\Repositories\UserRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class GetUserByIdJob
 *
 * @package App\Jobs\Frontend
 */
class UpdateUserByIdJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    protected $attributes;

    /**
     * @var int
     */
    protected $id;

    /**
     * UpdateUserJob constructor.
     *
     * @param int   $id
     * @param array $attributes
     */
    public function __construct(int $id, array $attributes)
    {
        $this->id = $id;
        $this->attributes = $attributes;
    }

    /**
     * @param \App\Repositories\UserRepository    $userRepository
     * @param \App\Repositories\ProfileRepository $profileRepository
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function handle(UserRepository $userRepository, ProfileRepository $profileRepository)
    {

        /** @var \App\Models\User $user */
        $user = $userRepository->updateById($this->id, $this->attributes);
        if (!empty($this->attributes['media'])) {
            $user->clearMediaCollectionExcept('avatar');
            $user->addMedia($this->attributes['media'])->toMediaCollection('avatar');
        }
        $profileAttributes = [];
        if (isset($this->attributes['profile'])) {
            $profileAttributes = $this->attributes['profile'];
        }
        if ($user->profile()->exists()) {
            $profileRepository->updateById($user->profile->id, $profileAttributes);
        } else {
            $profileRepository->createByUser($user, $profileAttributes);
        }

        return $userRepository->getById($user->id);
    }
}
