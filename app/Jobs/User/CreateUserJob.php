<?php

namespace App\Jobs\User;

use App\Repositories\ProfileRepository;
use App\Repositories\UserRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class CreateUserJob
 *
 * @package App\Jobs\Frontend
 */
class CreateUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    protected $attributes;

    /**
     * CreateUserJob constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @param \App\Repositories\UserRepository    $userRepository
     * @param \App\Repositories\ProfileRepository $profileRepository
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function handle(UserRepository $userRepository, ProfileRepository $profileRepository): \Illuminate\Database\Eloquent\Model {
        /** @var \App\Models\User $user */
        $user = $userRepository->create($this->attributes);

        if (!empty($this->attributes['media'])) {
            $user->addMedia($this->attributes['media'])->toMediaCollection('avatar');
        }

        $profileAttributes = [];
        if (isset($this->attributes['profile'])) {
            $profileAttributes = $this->attributes['profile'];
        }

        $profileRepository->createByUser($user, $profileAttributes);

        return $userRepository->getById($user->id);
    }
}
