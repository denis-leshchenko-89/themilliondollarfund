<?php

namespace App\Jobs\User;

use App\Repositories\UserRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class DeleteUserJob
 *
 * @package App\Jobs\User
 */
class DeleteUserByIdJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    protected $id;

    /**
     * DeleteUserJob constructor.
     *
     * @param $id
     */
    public function __construct($id) {
        $this->id = $id;
    }

    /**
     * @param \App\Repositories\UserRepository $userRepository
     *
     * @return bool|null
     * @throws \Exception
     */
    public function handle(UserRepository $userRepository): ?bool {
        return $userRepository->deleteById($this->id);
    }
}
