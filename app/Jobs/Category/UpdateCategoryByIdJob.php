<?php

namespace App\Jobs\Category;

use App\Repositories\CategoryRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class UpdateCategoryByIdJob
 *
 * @package App\Jobs\Category
 */
class UpdateCategoryByIdJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    protected $attributes;

    /**
     * @var int
     */
    protected $id;

    /**
     * constructor.
     *
     * @param int   $id
     * @param array $attributes
     */
    public function __construct(int $id, array $attributes)
    {
        $this->id = $id;
        $this->attributes = $attributes;
    }

    /**
     * @param \App\Repositories\CategoryRepository    $categoryRepository
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function handle(CategoryRepository $categoryRepository)
    {
        return $categoryRepository->updateById($this->id, $this->attributes);
    }
}
