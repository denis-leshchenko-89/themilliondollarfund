<?php

namespace App\Jobs\Category;

use App\Repositories\CategoryRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class GetCategoryByIdJob
 *
 * @package App\Jobs\Category
 */
class GetCategoryByIdJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    protected $id;

    /**
     * constructor.
     *
     * @param $id
     */
    public function __construct($id) {
        $this->id = $id;
    }

    /**
     * @param \App\Repositories\CategoryRepository $categoryRepository
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function handle(CategoryRepository $categoryRepository)
    {
        return $categoryRepository->getById($this->id);
    }
}
