<?php

namespace App\Jobs\Category;

use App\Repositories\CategoryRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class DeleteCategoryByIdJob
 *
 * @package App\Jobs\Category
 */
class DeleteCategoryByIdJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    protected $id;

    /**
     * constructor.
     *
     * @param $id
     */
    public function __construct($id) {
        $this->id = $id;
    }

    /**
     * @param \App\Repositories\CategoryRepository $categoryRepository
     *
     * @return bool|null
     * @throws \Exception
     */
    public function handle(CategoryRepository $categoryRepository): ?bool {
        return $categoryRepository->deleteById($this->id);
    }
}
