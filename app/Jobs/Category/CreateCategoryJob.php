<?php

namespace App\Jobs\Category;

use App\Repositories\CategoryRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class CreateCategoryJob
 *
 * @package App\Jobs\Frontend
 */
class CreateCategoryJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    protected $attributes;

    /**
     * constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @param \App\Repositories\CategoryRepository    $categoryRepository
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function handle(CategoryRepository $categoryRepository): \Illuminate\Database\Eloquent\Model {
        return $categoryRepository->create($this->attributes);
    }
}
