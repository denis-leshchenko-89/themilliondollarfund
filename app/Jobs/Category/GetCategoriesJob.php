<?php

namespace App\Jobs\Category;

use App\Repositories\CategoryRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class GetCategoriesJob
 *
 * @package App\Jobs\Category
 */
class GetCategoriesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    protected $attributes;

    /**
     * constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->attributes = $attributes;
    }

    /**
     * @param \App\Repositories\CategoryRepository $categoryRepository
     *
     * @return \Illuminate\Support\Collection
     */
    public function handle(CategoryRepository $categoryRepository)
    {
        return $categoryRepository->getByAttributes($this->attributes);
    }
}
