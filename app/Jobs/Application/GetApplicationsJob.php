<?php

namespace App\Jobs\Application;

use App\Repositories\ApplicationRepository;
use App\Repositories\CategoryRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class GetApplicationsJob
 *
 * @package App\Jobs\Application
 */
class GetApplicationsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    protected $attributes;

    /**
     * constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->attributes = $attributes;
    }

    /**
     * @param \App\Repositories\ApplicationRepository $applicationRepository
     *
     * @return \Illuminate\Support\Collection
     */
    public function handle(ApplicationRepository $applicationRepository)
    {
        return $applicationRepository->getByAttributes($this->attributes);
    }
}
