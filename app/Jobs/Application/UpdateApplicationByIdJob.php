<?php

namespace App\Jobs\Application;

use App\Models\Role;
use App\Models\User;
use App\Repositories\ApplicationRepository;
use App\Repositories\UserRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\MediaCollections\MediaRepository;

/**
 * Class UpdateApplicationByIdJob
 *
 * @package App\Jobs\Frontend
 */
class UpdateApplicationByIdJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    protected $attributes;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var User
     */
    protected $owner;

    /**
     * UpdateApplicationByIdJob constructor.
     *
     * @param int   $id
     * @param array $attributes
     * @param \App\Models\User|null $owner
     */
    public function __construct(int $id, array $attributes, ?User $owner = NULL)
    {
        $this->id = $id;
        $this->attributes = $attributes;
        $this->owner = $owner;
    }

    /**
     * @param \App\Repositories\ApplicationRepository $applicationRepository
     * @param \App\Repositories\UserRepository $userRepository
     * @param MediaRepository $mediaRepository
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function handle(ApplicationRepository $applicationRepository, UserRepository $userRepository, MediaRepository $mediaRepository)
    {
        $application = $applicationRepository->updateById($this->id, $this->attributes);

        if (isset($this->attributes['documents'])) {
            $documents = [];
            if (!empty($this->attributes['documents'])) {
                foreach ($this->attributes['documents'] as $document) {
                    if (!empty($document['id'])) {
                        $media = $mediaRepository->getByIds([$document['id']])->first();
                        if ($media and $media->model_id == $application->id) {
                            $documents[] = $media;
                        }
                    } else {
                        $documents[] = $application->addMedia($document['file'])->toMediaCollection('document');
                    }
                }
            }
            $application->clearMediaCollectionExcept('document', $documents);
        }

        if (isset($this->attributes['project_media'])) {
            $pictures = [];
            if (!empty($this->attributes['project_media'])) {
                $pictures[] = $application->addMedia($this->attributes['project_media'])->toMediaCollection('picture');
            }
            $application->clearMediaCollectionExcept('picture', $pictures);
        }

        $user_id_array = [];
        if ($this->owner) {
            foreach ($application->users()->wherePivot('type', Role::ROLE_FOUNDER)->get() as $user) {
                if ($user->access->role != Role::PERMISSION_OWNER or $this->owner->id != $user->id) {
                    continue;
                }
                $user_id_array[$user->id] = [
                    'type' => $user->access->type,
                    'role' => $user->access->role,
                ];
            }
        }

        if (isset($this->attributes['users'])) {
            foreach ($this->attributes['users'] as $key => $users) {
                foreach ($users as $user) {
                    if (empty($user['email'])) {
                        continue;
                    }
                    $userModel = $userRepository->getByColumn($user['email'], 'email');
                    if (!$userModel) {
                        $userModel = $userRepository->create(
                            [
                                'role'       => User::ENTREPRENEUR,
                                'name'       => $user['email'],
                                'email'      => $user['email'],
                                'password'   => Hash::make(Str::random(8)),
                                'is_invited' => TRUE,
                            ]
                        );
                    }
                    $user_id_array[$userModel->id] = [
                        'type' => $key,
                        'role' => $user['role'],
                    ];
                }
            }
        }
        $application->users()->sync($user_id_array);

        return $application;
    }
}
