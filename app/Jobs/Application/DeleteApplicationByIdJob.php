<?php

namespace App\Jobs\Application;

use App\Repositories\ApplicationRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class DeleteApplicationByIdJob
 *
 * @package App\Jobs\User
 */
class DeleteApplicationByIdJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    protected $id;

    /**
     * DeleteApplicationByIdJob constructor.
     *
     * @param $id
     */
    public function __construct($id) {
        $this->id = $id;
    }

    /**
     * @param \App\Repositories\ApplicationRepository $applicationRepository
     *
     * @return bool|null
     * @throws \Exception
     */
    public function handle(ApplicationRepository $applicationRepository): ?bool {
        return $applicationRepository->deleteById($this->id);
    }
}
