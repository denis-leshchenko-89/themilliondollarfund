<?php

namespace App\Jobs\Application;

use App\Models\Role;
use App\Models\User;
use App\Repositories\ApplicationRepository;
use App\Repositories\UserRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\MediaCollections\MediaRepository;

/**
 * Class CreateApplicationJob
 *
 * @package App\Jobs\Frontend
 */
class CreateApplicationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    protected $attributes;

    /**
     * @var User
     */
    protected $owner;

    /**
     * CreateApplicationJob constructor.
     *
     * @param array                 $attributes
     * @param \App\Models\User|null $owner
     */
    public function __construct(array $attributes, ?User $owner = NULL)
    {
        $this->attributes = $attributes;
        $this->owner = $owner;
    }

    /**
     * @param \App\Repositories\ApplicationRepository $applicationRepository
     * @param \App\Repositories\UserRepository $userRepository
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function handle(ApplicationRepository $applicationRepository, UserRepository $userRepository): \Illuminate\Database\Eloquent\Model {
        $application = $applicationRepository->create($this->attributes);

        if (isset($this->attributes['documents'])) {
            foreach ($this->attributes['documents'] as $document) {
                $application->addMedia($document['file'])->toMediaCollection('document');
            }
        }

        if (!empty($this->attributes['project_media'])) {
            $application->addMedia($this->attributes['project_media'])->toMediaCollection('picture');
        }

        if ($this->owner) {
            $this->attributes = array_merge_recursive($this->attributes, [
                'users' => [
                    Role::ROLE_FOUNDER => [
                        Role::PERMISSION_OWNER => [
                            'email' => $this->owner->email,
                            'role' => Role::PERMISSION_OWNER
                        ]
                    ]
                ]
            ]);
        }

        $user_id_array = [];
        foreach ($this->attributes['users'] as $key => $users) {
            foreach ($users as $user) {
                if (empty($user['email'])) {
                    continue;
                }
                $userModel = $userRepository->getByColumn($user['email'],'email');
                if (!$userModel) {
                    $userModel = $userRepository->create([
                        'role' => User::ENTREPRENEUR,
                        'name' => $user['email'],
                        'email' => $user['email'],
                        'password' => Hash::make(Str::random(8)),
                        'is_invited' => TRUE,
                    ]);
                }
                $user_id_array[$userModel->id] = [
                    'type' => $key,
                    'role' => $user['role'],
                ];
            }
        }

        $application->users()->sync($user_id_array);

        return $application;
    }
}
