<?php

namespace App\Jobs\Application;

use App\Repositories\ApplicationRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class GetApplicationByIdJob
 *
 * @package App\Jobs\Frontend
 */
class GetApplicationByIdJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    protected $id;

    /**
     * GetApplicationByIdJob constructor.
     *
     * @param $id
     */
    public function __construct($id) {
        $this->id = $id;
    }

    /**
     * @param \App\Repositories\ApplicationRepository $applicationRepository
     *
     * @return mixed
     */
    public function handle(ApplicationRepository $applicationRepository)
    {
        return $applicationRepository->getById($this->id);
    }
}
