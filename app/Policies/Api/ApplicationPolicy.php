<?php

namespace App\Policies\Api;

use App\Models\Application;
use App\Models\Role;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ApplicationPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param \App\Models\User $user
     *
     * @return bool
     */
    public function create(User $user): bool
    {
        return true;
    }

    /**
     * @param \App\Models\User        $user
     * @param \App\Models\Application $application
     *
     * @return bool
     */
    public function view(User $user, Application $application): bool
    {
        return $user->applications->contains($application->id);
    }

    /**
     * @param \App\Models\User        $user
     * @param \App\Models\Application $application
     *
     * @return bool
     */
    public function update(User $user, Application $application): bool
    {
        return $user->applications()->wherePivot('application_id', $application->id)->wherePivotIn('role', [Role::PERMISSION_ADMIN, Role::PERMISSION_OWNER])->exists();
    }

    /**
     * @param \App\Models\User        $user
     * @param \App\Models\Application $application
     *
     * @return bool
     */
    public function delete(User $user, Application $application): bool
    {
        return $user->applications()->wherePivot('application_id', $application->id)->wherePivotIn('role', [Role::PERMISSION_ADMIN, Role::PERMISSION_OWNER])->exists();
    }
}
