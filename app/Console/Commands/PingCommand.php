<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

/**
 * Class PingCommand
 * @package App\Console\Commands
 */
class PingCommand extends Command
{
    /**
     * @inheritdoc
     */
    protected $signature = 'ping';

    /**
     * @inheritdoc
     */
    protected $description = 'Simple console command for be sure artisan works correctly';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $this->output->writeln('Pong');

        return 0;
    }
}
